package com.dreamorbit.dynamitewtt.utilities

import android.app.Activity
import android.app.ProgressDialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.ConnectivityManager
import android.os.Build
import android.os.Environment
import android.util.Log
import android.util.Patterns
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.annotation.RequiresApi
import com.amazonaws.util.Md5Utils
import com.dreamorbit.dynamitewtt.application.AppConstants
import com.dreamorbit.dynamitewtt.application.PhAwareApplication
import com.dreamorbit.dynamitewtt.database.joinstudy.StudyRepository
import com.dreamorbit.dynamitewtt.database.registration.RegistartionRepository
import com.dreamorbit.dynamitewtt.network.ConnectivityReceiver
import com.dreamorbit.dynamitewtt.screens.welcome.WelcomeActivity
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.google.android.material.snackbar.Snackbar
import com.google.gson.GsonBuilder
import java.io.*
import java.nio.charset.StandardCharsets
import java.security.GeneralSecurityException
import java.security.MessageDigest
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import org.apache.commons.codec.binary.Base64


class Utility {

    var receiver: BroadcastReceiver? = null
    private var mProgressBar: ProgressDialog? = null
    private val TAG = "Utility"

    fun AgeValidationCheck(age: Int): AgeCheck? {
        return if (age >= 7 && age <= 17) {
            AgeCheck.assent
        } else if (age >= 18 && age <= 21) {
            AgeCheck.adult
        } else {
            AgeCheck.invalid
        }
    }

    fun isValidEmail(target: CharSequence?): Boolean {
        return if (target == null) {
            false
        } else {
            Patterns.EMAIL_ADDRESS.matcher(target).matches()
        }
    }

    /**
     * Returns a hexadecimal encoded SHA-256 hash for the input String.
     */
    @RequiresApi(Build.VERSION_CODES.KITKAT)
    fun getSHA256Hash(data: String): String? {
        val result: String? = null
        try {
            val digest =
                MessageDigest.getInstance("SHA-256")
            val hash =
                digest.digest(data.toByteArray(StandardCharsets.UTF_8))
            return bytesToHex(hash) // make it printable
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        return result
    }

    /**
     * convert byte array
     * to a hexadecimal string.
     *
     * @return string
     */
    private fun bytesToHex(hash: ByteArray): String? {
        val hexString = StringBuffer()
        for (i in hash.indices) {
            val hex = Integer.toHexString(0xff and hash[i].toInt())
            if (hex.length == 1) hexString.append('0')
            hexString.append(hex)
        }
        return hexString.toString()
    }

    /**
     * register broadcastreceiver for
     * OS >= Nougat
     */
    fun registerReciverForN(context: Context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            receiver = ConnectivityReceiver()
            context.registerReceiver(
                receiver,
                IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)
            )
        }
    }

    fun unRegisterReciverForN(context: Context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            context.unregisterReceiver(receiver)
            receiver = null
        }
    }


    /**
     * Snackbar utility method
     */
    fun showSnackBar(
        view: View?,
        msg: String?,
        duration: Int
    ) {
        val mySnackbar = Snackbar.make(
            view!!,
            msg!!, duration
        )
        mySnackbar.show()
    }

    /**
     * Hiding keyboard
     */
    fun hideKeyboard(view: View?) {
        if (view != null) {
            val imm =
                PhAwareApplication().getMyAppContext()!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    /**
     * Show progress bar
     */
    fun showProgressBar(
        context: Context?,
        message: String?
    ) {
        mProgressBar = ProgressDialog(context)
        mProgressBar!!.setCancelable(false)
        mProgressBar!!.setMessage(message)
        mProgressBar!!.setProgressStyle(ProgressDialog.STYLE_SPINNER)
        mProgressBar!!.isIndeterminate = true
        mProgressBar!!.show()
    }

    /**
     * Show progress bar
     */
    fun showProgressBarListen(
        context: Context?,
        message: String?
    ) {
        mProgressBar = ProgressDialog(context)
        mProgressBar!!.setCancelable(true)
        mProgressBar!!.setMessage(message)
        mProgressBar!!.setProgressStyle(ProgressDialog.STYLE_SPINNER)
        mProgressBar!!.isIndeterminate = true
        mProgressBar!!.show()
    }

    /**
     * Dismiss progress bar
     */
    fun dismissProgressBar() {
        if (mProgressBar != null && mProgressBar!!.isShowing) {
            mProgressBar!!.dismiss()
        }
    }

    private fun checkPlayServicesAvailable(activity: Activity) {
        val RC_PLAY_SERVICES = 123
        val availability = GoogleApiAvailability.getInstance()
        val resultCode =
            availability.isGooglePlayServicesAvailable(PhAwareApplication().getMyAppContext())
        if (resultCode != ConnectionResult.SUCCESS) {
            if (availability.isUserResolvableError(resultCode)) { // Show dialog to resolve the error.
                availability.getErrorDialog(activity, resultCode, RC_PLAY_SERVICES).show()
            } else { // Unresolvable error
                Toast.makeText(
                    PhAwareApplication().getMyAppContext(),
                    "Google Play Services error",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }
    }

    fun pullDatabase() {
        try {
            val file = Environment.getExternalStorageDirectory()
            val data = Environment.getDataDirectory()
            if (file.canWrite()) {
                val currentPath =
                    "/data/data/com.dreamorbit.walktalktrack/databases/Tasks.db"
                val copyPath = "Tasks.db"
                val currentDB = File(currentPath)
                val backupDB = File(file, copyPath)
                if (currentDB.exists()) {
                    val src =
                        FileInputStream(currentDB).channel
                    val dst =
                        FileOutputStream(backupDB).channel
                    dst.transferFrom(src, 0, src.size())
                    src.close()
                    dst.close()
                }
            }
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    /**
     * Get MD5 checksum of a upload encrypted zip file
     */
    fun getMD5(file: File?): String? {
        var MD5 = ""
        try {
            MD5 = Md5Utils.md5AsBase64(file)
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return MD5
    }

  /*  fun isPendingUpload(): Boolean {
        var pendingFiles = false
        val directories: Array<File> =
            SdkUtils.getAllDirectories(PhAwareApplication().getMyAppContext())
        for (dir in directories) {
            if (dir.isDirectory || dir.isFile) {
                pendingFiles = true
                break
            }
        }
        return pendingFiles
    }

    fun uploadPendingData() { //Check if any pending zip files available in app folder
        val directories: Array<File> =
            SdkUtils.getAllDirectories(PhAwareApplication().getMyAppContext())
        val email = SharedPrefSingleton.getInstance().userEmail
        val reportList: List<PostReportRequest> =
            ReportRepository.getInstance().getReport(email)
        if (ConnectivityReceiver.isConnected()) {
            if (reportList.size > 0 || directories != null && directories.size > 0) {
                Log.e("**Upload** ", "Pending")
                val appConstants = AppConstants()
                val intent =
                    Intent(PhAwareApplication().getMyAppContext(), BackGroundService::class.java)
                intent.putExtra(
                    appConstants.BACKGROUND_ACTION,
                    BackgroundAction.upload_all_pending.toString()
                )
                BackGroundService.enqueueBackgroundWork(
                    PhAwareApplication().getMyAppContext(),
                    intent
                )
            } else {
                Log.e("**Upload** ", "No Pendings")
            }
        }
    }

    fun sendTestSchedulerToWear() {
        val model: List<SchedulerModel> =
            SchedulerRepository.getInstance().getScheduler()
        if (model != null && model.size > 0) {
            Log.e("**Sending to Wear** ", "Yes Scheduler")
            val gb = GsonBuilder()
            gb.serializeNulls()
            val gson = gb.create()
            val currentScheduler = gson.toJson(model)
            MobileMessageSenderAsync().execute(TEST_SCHEDULER_ENABLE, currentScheduler)
            Log.e(TAG, "Mobile model sent to Wear $currentScheduler")
        } else {
            Log.e("**BottomBar** ", "No Scheduler")
        }
    }

    fun convert24To12Format(_24HourTime: String?): String? {
        return try {
            val _24HourSDF = SimpleDateFormat("HH:mm")
            val _12HourSDF: DateFormat = SimpleDateFormat("hh:mm aa")
            val _24HourDt = _24HourSDF.parse(_24HourTime)
            _12HourSDF.format(_24HourDt).replace(".", "")
        } catch (e: ParseException) {
            e.printStackTrace()
            ""
        }
    }

    fun convertHtmlPdf(docType: String): Boolean {
        val directory: File = SdkUtils.getPdfDirectory(
            SdkConstant.PDF_DIRECTORY.App,
            PhAwareApplication().getMyAppContext()
        )
        val filePdf = File(directory, "$docType.pdf")
        if (!directory.exists()) directory.mkdir()
        val htmlPath =
            File(PhAwareApplication().getMyAppContext().getFilesDir().getAbsolutePath().toString() + File.separator + SdkConstant.PH_HTML_DOCUMENTS)
        val htmlFile = File(htmlPath, "$docType.html")
        return try {
            val width =
                9.5f * 72 // inch * points (measurements are expressed in user units, and there are 72 user units in an inch)
            val height =
                12 * 72.toFloat() // inch * points (measurements are expressed in user units, and there are 72 user units in an inch)
            val pageSize = Rectangle(width, height)
            val document = Document(pageSize)
            val pdfWriter: PdfWriter =
                PdfWriter.getInstance(document, FileOutputStream(filePdf))
            document.open()
            XMLWorkerHelper.getInstance()
                .parseXHtml(pdfWriter, document, FileInputStream(htmlFile))
            document.close()
            Log.e("PDF Name", "document closed ")
            htmlFile.delete()
            Log.e("Alert", "Html file got deleted after converted to pdf")
            appendConsentNameToDoc(filePdf, docType)
        } catch (e: Exception) {
            Log.e("Exception: ", e.message.toString())
            false
        }
    }

    private fun appendConsentNameToDoc(
        srcPdf: File,
        docType: String
    ): Boolean {
        val directory: File = SdkUtils.getPdfDirectory(
            SdkConstant.PDF_DIRECTORY.App,
            PhAwareApplication().getMyAppContext()
        )
        val fileSignaturePdf = File(directory, docType + "_signature.pdf")
        // Name
        var firstName: String? = null
        var lastName: String? = null
        try {
            firstName = AESCrypt.decrypt(
                SdkConstant.KEY,
                SdkSharedPrefSingleton.getInstance().getFirstName()
            )
            lastName = AESCrypt.decrypt(
                SdkConstant.KEY,
                SdkSharedPrefSingleton.getInstance().getLastName()
            )
        } catch (e: GeneralSecurityException) {
            e.printStackTrace()
        }
        var consentName = "$firstName $lastName"
        //Signature
        val signature: String =
            SdkSharedPrefSingleton.getInstance().getSignature() //Signature from shared preference
        val decodedString =
            android.util.Base64.decode(signature, android.util.Base64.DEFAULT)
        val decodedBitmap =
            BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)
        val filteredBitmap = Bitmap.createScaledBitmap(decodedBitmap, 220, 100, false)
        val streamImage = ByteArrayOutputStream()
        filteredBitmap.compress(Bitmap.CompressFormat.PNG, 100, streamImage)
        var image: Image? = null
        try {
            image = Image.getInstance(streamImage.toByteArray())
        } catch (e: BadElementException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return try {
            val reader = PdfReader(srcPdf.toString())
            val dictionary: PdfDictionary = reader.getPageN(reader.getNumberOfPages() - 1)
            val `object`: PdfObject = dictionary.getDirectObject(PdfName.CONTENTS)
            if (`object` is PRStream) {
                val stream: PRStream = `object` as PRStream
                val data: ByteArray = PdfReader.getStreamBytes(stream)
                var dataString = String(data)
                consentName = "Print Name : $consentName"
                dataString = dataString.replace("Print Name :", consentName)
                stream.setData(dataString.toByteArray(charset("ISO-8859-2")))
            }
            *//* PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(fileSignaturePdf));

                PdfContentByte content = stamper.getOverContent(reader.getNumberOfPages());
                image.setAbsolutePosition(320f, 580f);

                content.addImage(image);

                stamper.close();
                reader.close();*//*
            val n: Int = reader.getNumberOfPages()
            val stamper = PdfStamper(reader, FileOutputStream(fileSignaturePdf))
            var pagecontent: PdfContentByte? = null
            var i = 0
            while (i < n) {
                pagecontent = stamper.getOverContent(++i)
                ColumnText.showTextAligned(
                    pagecontent, Element.ALIGN_CENTER,
                    Phrase(String.format("Page %s of %s", i, n)), 300, 30, 0
                )
            }
            image.setAbsolutePosition(320f, 580f)
            pagecontent.addImage(image)
            stamper.close()
            reader.close()
            //delete initial html converted pdf file
            srcPdf.delete()
            true
        } catch (e: IOException) {
            e.printStackTrace()
            false
        } catch (e: DocumentException) {
            e.printStackTrace()
            false
        }
    }*/

    /*public static void appendConsentNameToDoc(String docType) {
        String firstName = null, lastName = null;
        try {
            firstName = AESCrypt.decrypt(SdkConstant.KEY, SdkSharedPrefSingleton.getInstance().getFirstName());
            lastName = AESCrypt.decrypt(SdkConstant.KEY, SdkSharedPrefSingleton.getInstance().getLastName());
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
        }
        String consentName = firstName + " " + lastName;
        File directory = new File(PhAwareApplication().getMyAppContext().getFilesDir().getAbsolutePath() + File.separator + SdkConstant.PH_HTML_DOCUMENTS);
        File file = new File(directory, docType + ".html");

        InputStream is = null;
        BufferedReader reader;
        StringBuilder stringBuilder = new StringBuilder();
        if (file.exists()) {
            try {
                is = new FileInputStream(file);
                reader = new BufferedReader(new InputStreamReader(is));
                String line = reader.readLine();
                while (line != null) {
                    if (line.contains("Print Name :")) {
                        line = line.replace("Print Name :", "Print Name : " + consentName);
                    }
                    stringBuilder.append(line);
                    line = reader.readLine();
                }
            } catch (FileNotFoundException e1) {
                e1.printStackTrace();
            } catch (IOException e1) {
                e1.printStackTrace();
            }

            //Log.e("**Name Appended Html** ", stringBuilder.toString());
            writeToFile(stringBuilder, docType);
        }
    }

    private static void writeToFile(StringBuilder html, String docType) {
        //File directory = new File(PhAwareApplication().getMyAppContext().getFilesDir().getAbsolutePath() + File.separator + SdkConstant.PH_PDF_DOCUMENTS);
        File directory = Environment.getExternalStorageDirectory();
        File fileSignature = new File(directory, docType + "_signature.pdf");

        File tempPath = Environment.getExternalStorageDirectory();
        File tempFile = new File(tempPath, docType + "_name.pdf");

        try {

            float width = 8.5f * 72;   // inch * points (measurements are expressed in user units, and there are 72 user units in an inch)
            float height = 12 * 72;   // inch * points (measurements are expressed in user units, and there are 72 user units in an inch)
            Rectangle pageSize = new Rectangle(width, height);

            Document document = new Document();
            PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(tempFile));
            document.open();

            // Fixing xhtml tag
            Tidy tidy = new Tidy(); // obtain a new Tidy instance
            tidy.setXHTML(true); // set desired config options using tidy setters
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            tidy.setCharEncoding(Configuration.UTF8);
            tidy.parse(new ByteArrayInputStream(html.toString().getBytes()), output);
            String preparedText = output.toString("UTF-8");

            //Log.e("CHECKING", "JTidy writing: " + preparedText);

            InputStream inputStream = new ByteArrayInputStream(preparedText.getBytes());
            XMLWorkerHelper.getInstance().parseXHtml(pdfWriter, document, inputStream);

            Log.e("PDF Name", "JTidy Out done ");
            document.close();

            Log.e("PDF Name", "JTidy document closed ");

            String signature = SdkSharedPrefSingleton.getInstance().getSignature();//Signature from shared preference
            byte[] decodedString = Base64.decode(signature, Base64.DEFAULT);
            Bitmap decodedBitmap = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            Bitmap filteredBitmap = Bitmap.createScaledBitmap(decodedBitmap, 220, 100, false);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            filteredBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);

            Image image = Image.getInstance(stream.toByteArray());

            PdfReader pdfInputReader = new PdfReader(tempFile.toString());
            PdfStamper pdfOutputStamper = new PdfStamper(pdfInputReader, new FileOutputStream(fileSignature));
            PdfContentByte content = pdfOutputStamper.getOverContent(pdfInputReader.getNumberOfPages());
            image.setAbsolutePosition(180f, 200f);
            content.addImage(image);
            pdfOutputStamper.close();

            //tempFile.delete();

            Log.e("PDF Name", "JTidy Signature closed ");

        } catch (Exception e) {
            Log.e("Exception: ", e.getMessage().toString());
        }
    }*/

    /*public static void appendConsentNameToDoc(String docType) {
        String firstName = null, lastName = null;
        try {
            firstName = AESCrypt.decrypt(SdkConstant.KEY, SdkSharedPrefSingleton.getInstance().getFirstName());
            lastName = AESCrypt.decrypt(SdkConstant.KEY, SdkSharedPrefSingleton.getInstance().getLastName());
        } catch (GeneralSecurityException e) {
            e.printStackTrace();
        }
        String consentName = firstName + " " + lastName;
        File directory = new File(PhAwareApplication().getMyAppContext().getFilesDir().getAbsolutePath() + File.separator + SdkConstant.PH_HTML_DOCUMENTS);
        File file = new File(directory, docType + ".html");

        InputStream is = null;
        BufferedReader reader;
        StringBuilder stringBuilder = new StringBuilder();
        if (file.exists()) {
            try {
                is = new FileInputStream(file);
                reader = new BufferedReader(new InputStreamReader(is));
                String line = reader.readLine();
                while (line != null) {
                    if (line.contains("Print Name :")) {
                        line = line.replace("Print Name :", "Print Name : " + consentName);
                    }
                    stringBuilder.append(line);
                    line = reader.readLine();
                }
            } catch (FileNotFoundException e1) {
                e1.printStackTrace();
            } catch (IOException e1) {
                e1.printStackTrace();
            }

            //Log.e("**Name Appended Html** ", stringBuilder.toString());
            writeToFile(stringBuilder, docType);
        }
    }

    private static void writeToFile(StringBuilder html, String docType) {
        //File directory = new File(PhAwareApplication().getMyAppContext().getFilesDir().getAbsolutePath() + File.separator + SdkConstant.PH_PDF_DOCUMENTS);
        File directory = Environment.getExternalStorageDirectory();
        File fileSignature = new File(directory, docType + "_signature.pdf");

        File tempPath = Environment.getExternalStorageDirectory();
        File tempFile = new File(tempPath, docType + "_name.pdf");

        try {

            float width = 8.5f * 72;   // inch * points (measurements are expressed in user units, and there are 72 user units in an inch)
            float height = 12 * 72;   // inch * points (measurements are expressed in user units, and there are 72 user units in an inch)
            Rectangle pageSize = new Rectangle(width, height);

            Document document = new Document();
            PdfWriter pdfWriter = PdfWriter.getInstance(document, new FileOutputStream(tempFile));
            document.open();

            // Fixing xhtml tag
            Tidy tidy = new Tidy(); // obtain a new Tidy instance
            tidy.setXHTML(true); // set desired config options using tidy setters
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            tidy.setCharEncoding(Configuration.UTF8);
            tidy.parse(new ByteArrayInputStream(html.toString().getBytes()), output);
            String preparedText = output.toString("UTF-8");

            //Log.e("CHECKING", "JTidy writing: " + preparedText);

            InputStream inputStream = new ByteArrayInputStream(preparedText.getBytes());
            XMLWorkerHelper.getInstance().parseXHtml(pdfWriter, document, inputStream);

            Log.e("PDF Name", "JTidy Out done ");
            document.close();

            Log.e("PDF Name", "JTidy document closed ");

            String signature = SdkSharedPrefSingleton.getInstance().getSignature();//Signature from shared preference
            byte[] decodedString = Base64.decode(signature, Base64.DEFAULT);
            Bitmap decodedBitmap = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
            Bitmap filteredBitmap = Bitmap.createScaledBitmap(decodedBitmap, 220, 100, false);
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            filteredBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);

            Image image = Image.getInstance(stream.toByteArray());

            PdfReader pdfInputReader = new PdfReader(tempFile.toString());
            PdfStamper pdfOutputStamper = new PdfStamper(pdfInputReader, new FileOutputStream(fileSignature));
            PdfContentByte content = pdfOutputStamper.getOverContent(pdfInputReader.getNumberOfPages());
            image.setAbsolutePosition(180f, 200f);
            content.addImage(image);
            pdfOutputStamper.close();

            //tempFile.delete();

            Log.e("PDF Name", "JTidy Signature closed ");

        } catch (Exception e) {
            Log.e("Exception: ", e.getMessage().toString());
        }
    }*/
    /**
     * Clear shared preference and sqlite database and navigate to Welcome screen
     */
    fun signOut() {
        val intent =
            Intent(PhAwareApplication().getMyAppContext(), WelcomeActivity::class.java)
        intent.flags =
            Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        SharedPrefSingleton.getInstance().clearPhAwarePrefs(
            SharedPrefSingleton.ACCESS_TOKEN,
            SharedPrefSingleton.ACCESS_TOKEN,
            SharedPrefSingleton.COUNTRY,
            SharedPrefSingleton.EMAIL,
            SharedPrefSingleton.IS_USA
        )
        //ActivitiesRepository.getInstance().deleteActivities()
        //AdditionalInfoRepository.getInstance().deleteAdditionalInfo()
        //FlowRepository.getInstance().deleteFlow()
        StudyRepository().getInstance()!!.deleteStudyData()
        //QuestionsRepository.getInstance().deleteQuestions()
        RegistartionRepository().getInstance()!!.deleteRegistrationData()
        //SchedulerRepository.getInstance().deleteSchedulerData()
        PhAwareApplication().getMyAppContext()!!.startActivity(intent)
        //delete html and pdf directory files
    }

    /**
     * Cancel all the local alarm once logout
     */
    fun clearAlarms() {
       /* NotificationHelper.cancelRatingRTCNotification() // Rating alarm canceled
        //NotificationHelper.cancelResetRTCNotification();  // Survey reset canceled
        MyFirebaseMessagingService.cancelSchedulerRTCNotification() // Scheduler alarm canceled
        //Cancel GCM network manager task
        GcmNetworkManager.getInstance(PhAwareApplication().getMyAppContext()).cancelAllTasks(
            PhGCMTaskService::class.java
        )*/
    }

    fun encodeFileToBase64Binary(fileName: File): String? {
        val bytes = loadFile(fileName)
        val encoded: ByteArray = Base64.encodeBase64(bytes)
        return String(encoded)
    }

    private fun loadFile(file: File): ByteArray? {
        var `is`: InputStream? = null
        var bytes: ByteArray? = null
        try {
            `is` = FileInputStream(file)
            val length = file.length()
            if (length > Int.MAX_VALUE) { // File is too large
            }
            bytes = ByteArray(length.toInt())
            var offset = 0
            var numRead = 0
            while (offset < bytes.size
                && `is`.read(bytes, offset, bytes.size - offset).also {
                    numRead = it
                } >= 0
            ) {
                offset += numRead
            }
            if (offset < bytes.size) {
                throw IOException("Could not completely read file " + file.name)
            }
            `is`.close()
        } catch (e: FileNotFoundException) {
            e.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        }
        return bytes
    }

    enum class AgeCheck {
        assent, adult, invalid
    }

    enum class BackgroundAction {
        upload_survey, reset_survey, upload_walk_test, upload_all_pending, upload_wear_data, upload_summary_data
    }

    enum class PARTIAL_SCREEN {
        signup, consent, assent, user_profile, complete
    }
}