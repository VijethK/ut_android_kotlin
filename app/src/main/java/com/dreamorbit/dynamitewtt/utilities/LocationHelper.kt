package com.dreamorbit.dynamitewtt.utilities

import android.Manifest
import android.app.Activity
import android.content.ContentValues
import android.content.Context
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import android.os.Looper
import android.util.Log
import android.widget.Toast
import androidx.core.app.ActivityCompat
import com.dreamorbit.dynamitewtt.application.PhAwareApplication
import com.dreamorbit.dynamitewtt.network.ConnectivityReceiver
import com.dreamorbit.dynamitewtt.pojo.location.Coordinate
import com.dreamorbit.dynamitewtt.pojo.location.Item
import com.dreamorbit.dynamitewtt.pojo.location.LocationData
import com.dreamorbit.dynamitewtt.screens.splash.GeoCodeApi
import com.dreamorbit.dynamitewtt.screens.splash.SplashActivity
import com.google.android.gms.location.*
import com.google.android.gms.tasks.OnSuccessListener
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class LocationHelper {
            /**
         * The desired interval for location updates. Inexact. Updates may be more or less frequent.
         */
        val UPDATE_INTERVAL_IN_MILLISECONDS: Long = 10000
        /**
         * The fastest rate for active location updates. Exact. Updates will never be more frequent
         * than this value.
         */
        val FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS =
            UPDATE_INTERVAL_IN_MILLISECONDS / 2
        /**
         * Constant used in the location settings dialog.
         */
        val REQUEST_CHECK_SETTINGS = 0x1
        var mCurrentLocation: Location? = null
        var locationManager: LocationManager? = null
        var mLocationCallback: LocationCallback? = null
        var mFusedLocationClient: FusedLocationProviderClient? = null
        var permissionsUtility: PermissionsUtility? = null
        /**
         * Provides access to the Location Settings API.
         */
        var mSettingsClient: SettingsClient? = null
        /**
         * Stores the types of location services the client is interested in using. Used for checking
         * settings to determine if the device has optimal location settings.
         */
        var mLocationSettingsRequest: LocationSettingsRequest? = null
        /**
         * Stores parameters for requests to the FusedLocationProviderApi.
         */
        var mLocationRequest: LocationRequest? = null
        /**
         * Tracks the status of the location updates request. Value changes when the user presses the
         * Start Updates and Stop Updates buttons.
         */
        var mRequestingLocationUpdates: Boolean? = null
        var activity: Activity? = null

        //Storing location details
        val locationData = LocationData()

        fun LocationHelper(activity: Activity) {
            this.activity = activity
            // Kick off the process of building the LocationCallback, LocationRequest, and
// LocationSettingsRequest objects.
            initLocationManager()
            createLocationCallback()
            createLocationRequest()
            buildLocationSettingsRequest()
        }

        open fun initLocationManager() {
            permissionsUtility = PermissionsUtility
            val phawareApplication = PhAwareApplication()
            locationManager =
                phawareApplication.getMyAppContext()!!.getSystemService(Context.LOCATION_SERVICE) as LocationManager?
            mFusedLocationClient =
                LocationServices.getFusedLocationProviderClient(phawareApplication.getMyAppContext()!!)
            mSettingsClient =
                LocationServices.getSettingsClient(phawareApplication.getMyAppContext()!!)
            mRequestingLocationUpdates = false
        }
    private fun createLocationCallback() {
        mLocationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult) {
                super.onLocationResult(locationResult)
                mCurrentLocation = locationResult.lastLocation
                //SharedPrefSingleton.getInstance().saveString(AppConstants.LATITUDE,String.valueOf(mCurrentLocation.getLatitude()));
//SharedPrefSingleton.getInstance().saveString(AppConstants.LONGITUDE,String.valueOf(mCurrentLocation.getLongitude()));/* SdkSharedPrefSingleton.getInstance()
//                    .saveLatitude(mCurrentLocation!!.getLatitude().toString())
//                SdkSharedPrefSingleton.getInstance()
//                    .saveLongitude(mCurrentLocation!!.getLongitude().toString())
//                if (activity is SurveyActivity) {
//                    val item = Item()
//                    val coordinate = Coordinate()
//                    coordinate.setLatitude(mCurrentLocation!!.getLatitude())
//                    coordinate.setLongitude(mCurrentLocation!!.getLongitude())
//                    item.setCoordinate(coordinate)
//                    item.setAltitude(mCurrentLocation!!.getAltitude())
//                    item.setHorizontalAccuracy(mCurrentLocation!!.getAccuracy())
//                    item.setSpeed(mCurrentLocation!!.getSpeed())
//                    item.setCourse(0.0)
//                    item.setVerticalAccuracy(0)
//                    val dateFormatter: DateFormat =
//                        SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ssZ")
//                    val currentDate = Date()
//                    item.setTimestamp(dateFormatter.format(currentDate))
//                    locationData.addItems(item)
//                    Toast.makeText(
//                        activity,
//                        "Latitude: " + mCurrentLocation!!.getAltitude() + " Longitude: " + mCurrentLocation!!.getLongitude() +
//                                "\n vertcal: " + mCurrentLocation!!.getAccuracy() + " horizontal: " + mCurrentLocation!!.getAccuracy() +
//                                "\n speed: " + mCurrentLocation!!.getSpeed() + " altitude: " + mCurrentLocation!!.getAltitude()
//                                + "\n date: " + dateFormatter.format(currentDate),
//                        Toast.LENGTH_SHORT
//                    ).show()
//                } else {
//                    getAddressFromLocation(
//                        mCurrentLocation!!.getLatitude(),
//                        mCurrentLocation!!.getLongitude()
//                    )
//                }*/

                getAddressFromLocation(mCurrentLocation!!.getLatitude(),mCurrentLocation!!.getLongitude())

            }
        }
    }

    /***
     * Geting address using latitude and longitude
     * @param lat
     * @param longitude
     */
    private fun getAddressFromLocation(
        lat: Double,
        longitude: Double
    ) {
        val connectivityreceiver = ConnectivityReceiver()
        if (connectivityreceiver.isConnected()) {
            if (lat != 0.0 && longitude != 0.0) {
                stopLocationUpdates(activity)
              //  GeoCodeApi().getLocation(this, lat, longitude)
            }
        } else {
            toWelcome()
        }
    }

    fun getLocationDataOf6minWalk(): LocationData? {
        return locationData
    }

    private fun createLocationRequest() {
        mLocationRequest = LocationRequest()
        // Sets the desired interval for active location updates. This interval is
// inexact. You may not receive updates at all if no location sources are available, or
// you may receive them slower than requested. You may also receive updates faster than
// requested if other applications are requesting location at a faster interval.
        mLocationRequest!!.interval = UPDATE_INTERVAL_IN_MILLISECONDS
        // Sets the fastest rate for active location updates. This interval is exact, and your
// application will never receive updates faster than this value.
        mLocationRequest!!.fastestInterval = FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS
        mLocationRequest!!.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
    }

    private fun buildLocationSettingsRequest() {
        val builder = LocationSettingsRequest.Builder()
        builder.addLocationRequest(mLocationRequest!!)
        mLocationSettingsRequest = builder.build()
    }


    /**
     * Requests location updates from the FusedLocationApi. Note: we don't call this unless location
     * runtime permission has been granted.
     */
    fun startLocationUpdates() { //Dont check for location if allready saved in preference
        if (SharedPrefSingleton.getInstance()!!.getBoolean(SharedPrefSingleton.IS_USA, false)) {
            return
        }
        mRequestingLocationUpdates = true
        // Begin by checking if the device has the necessary location settings.
        mSettingsClient!!.checkLocationSettings(mLocationSettingsRequest)
            .addOnSuccessListener(activity!!, OnSuccessListener {
                Log.i(ContentValues.TAG, "All location settings are satisfied.")
                if (ActivityCompat.checkSelfPermission(
                        activity!!,
                        Manifest.permission.ACCESS_FINE_LOCATION
                    ) !== PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                        activity!!,
                        Manifest.permission.ACCESS_COARSE_LOCATION
                    ) !== PackageManager.PERMISSION_GRANTED
                ) { // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return@OnSuccessListener
                }
                mFusedLocationClient!!.requestLocationUpdates(
                    mLocationRequest,
                    mLocationCallback, Looper.myLooper()
                )
            })
    }
   /* fun geoCodeResult(geoResponse: GeoResponse) {
        if (geoResponse.getResults().get(0).getAddressComponents().get(
                geoResponse.getResults().get(
                    0
                ).getAddressComponents().size() - 2
            ).getLongName().equalsIgnoreCase("United States")
        ) {
            SharedPrefSingleton.getInstance().saveString(
                SharedPrefSingleton.COUNTRY,
                geoResponse.getResults().get(0).getAddressComponents().get(
                    geoResponse.getResults().get(
                        0
                    ).getAddressComponents().size() - 2
                ).getLongName()
            )
            SharedPrefSingleton.getInstance()!!.saveBoolean(SharedPrefSingleton.IS_USA, true)
            if (activity is JoinStudyActivity) {
                (activity as JoinStudyActivity).deviceLocality()
            }
        } else {
            SharedPrefSingleton.getInstance()!!.saveBoolean(SharedPrefSingleton.IS_USA, false)
        }
        toWelcome()
    }

    fun geoCodeFailure() {
        toWelcome()
    }

    fun geoCodeError() {
        toWelcome()
    }*/

    private fun toWelcome() {
        stopLocationUpdates(activity)
        if (activity is SplashActivity) {
            (activity as SplashActivity).navigationFlow()
        }
        //gotoWelcome();
    }
    protected fun stopLocationUpdates(activity: Activity?) {
        if (!mRequestingLocationUpdates!!) {
            Log.d(
                ContentValues.TAG,
                "stopLocationUpdates: updates never requested, no-op."
            )
            return
        }
        // It is a good practice to remove location requests when the activity is in a paused or
// stopped state. Doing so helps battery performance and is especially
// recommended in applications that request frequent location updates.
        mFusedLocationClient!!.removeLocationUpdates(mLocationCallback)
            .addOnCompleteListener(
                activity!!
            ) { }
        Log.d("", "Location update stopped .......................")
    }


}