package com.dreamorbit.dynamitewtt.utilities

import android.Manifest
import android.app.Activity
import android.content.pm.PackageManager
import android.os.Build
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat


object PermissionsUtility {
    public val PERMISSIONS_REQUEST = 154

    public fun hasPermissions(activity: Activity?, vararg permissions: String?): Boolean {
        if (Build.VERSION.SDK_INT < 23) {
            return true
        }
        for (permission in permissions) {
                if (ContextCompat.checkSelfPermission(
                        activity!!,
                        permission!!
                    ) !== PackageManager.PERMISSION_GRANTED
                ) {
                    return false
                }
            }
        return true
    }

    fun shouldShowRationale(
        activity: Activity?,
        vararg permissions: String?
    ): Boolean {
        if (Build.VERSION.SDK_INT < 23) {
            return true
        }
        for (permission in permissions) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity!!, permission!!)) {
                return true
            }
        }
        return false
    }
    fun getPermissions(activity: Activity?, permissions: Array<String?>?) {
            ActivityCompat.requestPermissions(activity!!, permissions!!, PERMISSIONS_REQUEST)
    }

    public fun requestForLocationPermission(activity: Activity?) {
        if (ContextCompat.checkSelfPermission(
                activity!!,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) !== PackageManager.PERMISSION_GRANTED
        ) {
            getPermissions(
                activity,
                arrayOf(
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                )
            )
        }
    }

    fun requestForCallPermission(activity: Activity?) {
        if (ContextCompat.checkSelfPermission(
                activity!!,
                Manifest.permission.CALL_PHONE
            ) !== PackageManager.PERMISSION_GRANTED
        ) {
            getPermissions(
                activity,
                arrayOf(Manifest.permission.CALL_PHONE)
            )
        }
    }
}