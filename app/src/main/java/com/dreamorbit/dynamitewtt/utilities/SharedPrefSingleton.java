package com.dreamorbit.dynamitewtt.utilities;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.dreamorbit.dynamitewtt.pojo.register.StudyConfiguration;
import com.google.gson.Gson;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

public class SharedPrefSingleton{

    //Mujasam
    public static final String EMAIL = "email";

    //AccessToken
    public static final String ACCESS_TOKEN = "ACCESS_TOKEN";
    public static final String REFRESH_TOKEN = "REFRESH_TOKEN";

    //Public Key Aws url
    public static final String PUBLIC_KEY_AWS_URL = "aws_url";

    //Public Key
    public static final String PUBLIC_KEY = "public_key";

    //IS USA country region
    public static final String IS_USA = "IS_USA";
    public static final String COUNTRY = "COUNTRY";
    public static final String CASTED_DATA = "casted_data";
    //public static final String TEMP_TOKN = "eyJraWQiOiJZd3J3Z3VoTXhTUWM2djVob1FGVWRFZVRcL2dZN3UyTU5hWjVzM1ZRZGZCND0iLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJmMjVjNTAwMy1kNTk0LTRjNDgtYTkyMy1jYmNmZjJiNmIyMjkiLCJ0b2tlbl91c2UiOiJhY2Nlc3MiLCJzY29wZSI6ImF3cy5jb2duaXRvLnNpZ25pbi51c2VyLmFkbWluIiwiaXNzIjoiaHR0cHM6XC9cL2NvZ25pdG8taWRwLnVzLWVhc3QtMS5hbWF6b25hd3MuY29tXC91cy1lYXN0LTFfNW14SzRHZDN1IiwiZXhwIjoxNTA5NzA3NTgyLCJpYXQiOjE1MDk3MDM5ODIsImp0aSI6IjgxMGFkYjBhLTkzNzYtNGUwZC1hNjMyLTRlMTJkMzhjN2QzYyIsImNsaWVudF9pZCI6Im8zNGZoZm1wdWVxM3RuOWluMGJ1bGEwNHEiLCJ1c2VybmFtZSI6IjM2bGdhNTBzNjc4NzgyNmI0dDQ2NzAwMHBlOHBjMDg1MGFmNzczNGUzZDczZWNjMWE5YTUxMjBmOTVmNDFjZDRzaEBwaGF3YXJlLm9yZyJ9.IIR8WA63UrrwVlm8noWs0EP_R59KpzjHfzf4TGWrchxwC73xZkRAqMbUNTjHJX0-cQsR0KwTFwO4fRTA-n7PjsvWxRMQEKiqsu9z11fTxiR8H3QzJFdDJw_jkSkWa6c8s0A5uPJZEYoRiD6_sapyZs2whhAT8Ke6QKHyy8jUrK4kbhrqT9Z67wX3WYZfSentm2EBA-RFIP3hPFgfLQ1-_ON-MlMDc3fFCTGL5pmKfk4JFBqf2g3zih9SjKSs29y6Kl_tnk5l9QHksTjCrpZ-9GvwSZ5LMN9_nV54EROimVPXiPLcGPsTzHiKh7UNQsMwVN3L-T_uso2_ApKrdOIR_g";

    public static final String RATING_COUNT = "RATING_COUNT";
    private static final String FCM_TOKEN = "fcm_token";

    private static final String STUDY_CONFIG = "study_config";

    private static SharedPrefSingleton ourInstance;
    SharedPreferences sharedPreferences;

    private SharedPrefSingleton() {
    }

    public static SharedPrefSingleton getInstance() {
        if (ourInstance == null) {
            ourInstance = new SharedPrefSingleton();
        }
        return ourInstance;
    }

    public void initialize(Context context) {
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
    }

    private SharedPreferences.Editor getEditor() {
        return sharedPreferences.edit();
    }

    public void saveBoolean(@TAG String tag, boolean state) {
        getEditor().putBoolean(tag, state).apply();
    }

    public boolean getBoolean(@TAG String tag, boolean defaultValue) {
        return sharedPreferences.getBoolean(tag, defaultValue);
    }

    public void saveString(@TAG String tag, String value) {
        getEditor().putString(tag, value).apply();
    }

    public String getString(@TAG String tag, String defaultValue) {
        return sharedPreferences.getString(tag, defaultValue);
    }

    public void saveLong(@TAG String tag, long value) {
        getEditor().putLong(tag, value).apply();
    }

    public long getLong(@TAG String tag, long defaultValue) {
        return sharedPreferences.getLong(tag, defaultValue);
    }

    public void saveInt(@TAG String tag, int value) {
        getEditor().putInt(tag, value).apply();
    }

    public int getInt(@TAG String tag, int defaultValue) {
        return sharedPreferences.getInt(tag, defaultValue);
    }

    public void clearPhAwarePrefs(@TAG String... tags) {
        for (String tag : tags) {
            getEditor().remove(tag).apply();
        }
    }

    public void clearCalendarPrefs(@TAG String... tags) {
        for (String tag : tags) {
            getEditor().remove(tag).apply();
        }
    }

    public void clearStateTamadPrefs(@TAG String... tags) {
        for (String tag : tags) {
            getEditor().remove(tag).apply();
        }
    }

    public void savePublicKeyUrl(String value) {
        getEditor().putString(PUBLIC_KEY_AWS_URL, value).apply();
    }

    public String getPublicKeyUrl() {
        return sharedPreferences.getString(PUBLIC_KEY_AWS_URL, "");
    }

    public void savePublicKey(String value) {
        getEditor().putString(PUBLIC_KEY, value).apply();
    }

    public String getPublicKey() {
        return sharedPreferences.getString(PUBLIC_KEY, "");
    }

    public void saveCastData(String message) {
        getEditor().putString(CASTED_DATA, message).apply();
    }

    public String getCastedData() {
        return sharedPreferences.getString(CASTED_DATA, "");
    }

    public void saveRateCount(int count) {
        getEditor().putInt(RATING_COUNT, count).apply();
    }

    public int getRateCount() {
        return sharedPreferences.getInt(RATING_COUNT, 0);
    }

    public String getUserEmail() {
        return sharedPreferences.getString(EMAIL, "");
    }

    public void saveUserEmail(String userEmail) {
        getEditor().putString(EMAIL, userEmail).apply();
    }

    public void saveFcmToken(String refreshedToken) {
        getEditor().putString(FCM_TOKEN, refreshedToken).apply();
    }

    public String getFcmToken() {
        return sharedPreferences.getString(FCM_TOKEN, "");
    }

    public void saveStudyConfig(StudyConfiguration studyConfiguration) {
        Gson gson = new Gson();
        String studyConfig = gson.toJson(studyConfiguration, StudyConfiguration.class);
        getEditor().putString(STUDY_CONFIG, studyConfig).apply();
    }

    public StudyConfiguration getStudyConfig() {
        Gson gson = new Gson();
        StudyConfiguration studyConfig = gson.fromJson(sharedPreferences.getString(STUDY_CONFIG, ""), StudyConfiguration.class);
        return studyConfig;
    }

    @Retention(RetentionPolicy.SOURCE)
    public @interface TAG {
    }
}