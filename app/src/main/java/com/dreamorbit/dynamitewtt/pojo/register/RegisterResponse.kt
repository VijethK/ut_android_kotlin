package com.dreamorbit.dynamitewtt.pojo.register

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class RegisterResponse {
    @SerializedName("birthdate")
    @Expose
    private var birthdate: String? = null
    @SerializedName("access_token")
    @Expose
    private var accessToken: String? = null
    @SerializedName("expires_in")
    @Expose
    private var expiresIn: Int? = null
    @SerializedName("token_type")
    @Expose
    private var tokenType: String? = null
    @SerializedName("refresh_token")
    @Expose
    private var refreshToken: String? = null
    @SerializedName("id_token")
    @Expose
    private var idToken: String? = null
    @SerializedName("new_device_metadata")
    @Expose
    private var newDeviceMetadata: Any? = null
    @SerializedName("registration_status")
    @Expose
    private var registrationStatus: String? = null
    @SerializedName("error")
    @Expose
    private var errorMessage: String? = null
    @SerializedName("study_configuration")
    @Expose
    private var studyConfiguration: StudyConfiguration? = null

    fun RegisterResponse(
        accessToken: String?,
        expiresIn: Int?,
        tokenType: String?,
        refreshToken: String?,
        idToken: String?,
        newDeviceMetadata: Any?,
        errorMessage: String?
    ) {
        this.accessToken = accessToken
        this.expiresIn = expiresIn
        this.tokenType = tokenType
        this.refreshToken = refreshToken
        this.idToken = idToken
        this.newDeviceMetadata = newDeviceMetadata
        this.errorMessage = errorMessage
    }

    fun RegisterResponse() {}

    fun getBirthdate(): String? {
        return birthdate
    }

    fun setBirthdate(birthdate: String?) {
        this.birthdate = birthdate
    }

    fun getAccessToken(): String? {
        return accessToken
    }

    fun setAccessToken(accessToken: String?) {
        this.accessToken = accessToken
    }

    fun getExpiresIn(): Int? {
        return expiresIn
    }

    fun setExpiresIn(expiresIn: Int?) {
        this.expiresIn = expiresIn
    }

    fun getTokenType(): String? {
        return tokenType
    }

    fun setTokenType(tokenType: String?) {
        this.tokenType = tokenType
    }

    fun getRefreshToken(): String? {
        return refreshToken
    }

    fun setRefreshToken(refreshToken: String?) {
        this.refreshToken = refreshToken
    }

    fun getIdToken(): String? {
        return idToken
    }

    fun setIdToken(idToken: String?) {
        this.idToken = idToken
    }

    fun getNewDeviceMetadata(): Any? {
        return newDeviceMetadata
    }

    fun setNewDeviceMetadata(newDeviceMetadata: Any?) {
        this.newDeviceMetadata = newDeviceMetadata
    }

    fun getErrorMessage(): String? {
        return errorMessage
    }

    fun setErrorMessage(errorMessage: String?) {
        this.errorMessage = errorMessage
    }

    fun getRegistrationStatus(): String? {
        return registrationStatus
    }

    fun setRegistrationStatus(registrationStatus: String?) {
        this.registrationStatus = registrationStatus
    }

    fun getStudyConfiguration(): StudyConfiguration? {
        return studyConfiguration
    }

    fun setStudyConfiguration(studyConfiguration: StudyConfiguration?) {
        this.studyConfiguration = studyConfiguration
    }
}