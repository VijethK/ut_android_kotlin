package com.dreamorbit.dynamitewtt.pojo.additionalInfo

class AdditionalInfoRequest {

    private var actual_email: String? = null
    private var gender: String? = null
    private var height: String? = null
    private var weight: String? = null
    private var diagnosed_with_ph: String? = null
    private var medication_for_ph: String? = null
    private var sync_wearable: String? = null
    private var survey_start_date: String? = null
    private var survey_end_date: String? = null
    private var birthdate: String? = null

    fun AdditionalInfoRequest() {}

    fun getBirthdate(): String? {
        return birthdate
    }

    fun setBirthdate(birthdate: String?) {
        this.birthdate = birthdate
    }

    fun getActual_email(): String? {
        return actual_email
    }

    fun setActual_email(actual_email: String?) {
        this.actual_email = actual_email
    }

    fun getGender(): String? {
        return gender
    }

    fun setGender(gender: String?) {
        this.gender = gender
    }

    fun getHeight(): String? {
        return height
    }

    fun setHeight(height: String?) {
        this.height = height
    }

    fun getWeight(): String? {
        return weight
    }

    fun setWeight(weight: String?) {
        this.weight = weight
    }

    fun getDiagnosed_with_ph(): String? {
        return diagnosed_with_ph
    }

    fun setDiagnosed_with_ph(diagnosed_with_ph: String?) {
        this.diagnosed_with_ph = diagnosed_with_ph
    }

    fun getMedication_for_ph(): String? {
        return medication_for_ph
    }

    fun setMedication_for_ph(medication_for_ph: String?) {
        this.medication_for_ph = medication_for_ph
    }

    fun getSync_wearable(): String? {
        return sync_wearable
    }

    fun setSync_wearable(sync_wearable: String?) {
        this.sync_wearable = sync_wearable
    }

    fun getSurvey_start_date(): String? {
        return survey_start_date
    }

    fun setSurvey_start_date(survey_start_date: String?) {
        this.survey_start_date = survey_start_date
    }

    fun getSurvey_end_date(): String? {
        return survey_end_date
    }

    fun setSurvey_end_date(survey_end_date: String?) {
        this.survey_end_date = survey_end_date
    }
}