package com.dreamorbit.dynamitewtt.pojo.location

import java.util.*

class Coordinate {
    private var longitude: Double? = null
    private var latitude: Double? = null
    private val additionalProperties: MutableMap<String, Any> =
        HashMap()

    fun getLongitude(): Double? {
        return longitude
    }

    fun setLongitude(longitude: Double?) {
        this.longitude = longitude
    }

    fun getLatitude(): Double? {
        return latitude
    }

    fun setLatitude(latitude: Double?) {
        this.latitude = latitude
    }

    fun getAdditionalProperties(): Map<String, Any>? {
        return additionalProperties
    }

    fun setAdditionalProperty(name: String, value: Any) {
        additionalProperties[name] = value
    }
}