package com.dreamorbit.dynamitewtt.pojo.location

class Item {
    private var verticalAccuracy: Int? = null
    private var course: Double? = null
    private var speed: Float? = null
    private var horizontalAccuracy: Float? = null
    private var timestamp: String? = null
    private var altitude: Double? = null
    private var coordinate: Coordinate? = null

    fun getVerticalAccuracy(): Int? {
        return verticalAccuracy
    }

    fun setVerticalAccuracy(verticalAccuracy: Int?) {
        this.verticalAccuracy = verticalAccuracy
    }

    fun getCourse(): Double? {
        return course
    }

    fun setCourse(course: Double?) {
        this.course = course
    }

    fun getSpeed(): Float? {
        return speed
    }

    fun setSpeed(speed: Float?) {
        this.speed = speed
    }

    fun getHorizontalAccuracy(): Float? {
        return horizontalAccuracy
    }

    fun setHorizontalAccuracy(horizontalAccuracy: Float?) {
        this.horizontalAccuracy = horizontalAccuracy
    }

    fun getTimestamp(): String? {
        return timestamp
    }

    fun setTimestamp(timestamp: String?) {
        this.timestamp = timestamp
    }

    fun getAltitude(): Double? {
        return altitude
    }

    fun setAltitude(altitude: Double?) {
        this.altitude = altitude
    }

    fun getCoordinate(): Coordinate? {
        return coordinate
    }

    fun setCoordinate(coordinate: Coordinate?) {
        this.coordinate = coordinate
    }

}