package com.dreamorbit.dynamitewtt.pojo.register

class RegisterRequest {
    private var email: String? = null
    private var password: String? = null
    private var name: String? = null
    private var role: String? = null
    private var device_token: String? = null
    private var device_type: String? = null
    private var actual_email: String? = null
    private var survey_start_date: String? = null
    private var survey_end_date: String? = null

    fun RegisterRequest() {}

    fun getDevice_token(): String? {
        return device_token
    }

    fun setDevice_token(device_token: String?) {
        this.device_token = device_token
    }

    fun getDevice_type(): String? {
        return device_type
    }

    fun setDevice_type(device_type: String?) {
        this.device_type = device_type
    }

    fun getEmail(): String? {
        return email
    }

    fun setEmail(email: String?) {
        this.email = email
    }

    fun getPassword(): String? {
        return password
    }

    fun setPassword(password: String?) {
        this.password = password
    }

    fun getName(): String? {
        return name
    }

    fun setName(name: String?) {
        this.name = name
    }

    fun getRole(): String? {
        return role
    }

    fun setRole(role: String?) {
        this.role = role
    }

    fun getActual_email(): String? {
        return actual_email
    }

    fun setActual_email(actual_email: String?) {
        this.actual_email = actual_email
    }

    fun getSurvey_start_date(): String? {
        return survey_start_date
    }

    fun setSurvey_start_date(survey_start_date: String?) {
        this.survey_start_date = survey_start_date
    }

    fun getSurvey_end_date(): String? {
        return survey_end_date
    }

    fun setSurvey_end_date(survey_end_date: String?) {
        this.survey_end_date = survey_end_date
    }
}