package com.dreamorbit.dynamitewtt.pojo.location

import java.util.*

class LocationData {
    private val items: MutableList<Item> = ArrayList()

    fun getItems(): List<Item>? {
        return items
    }

    fun addItems(item: Item) {
        items.add(item)
    }
}
