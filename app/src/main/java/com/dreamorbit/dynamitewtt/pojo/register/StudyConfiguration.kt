package com.dreamorbit.dynamitewtt.pojo.register

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class StudyConfiguration {

    @SerializedName("id")
    @Expose
    private var id: Int? = null
    @SerializedName("study_consent")
    @Expose
    private var studyConsent: Boolean? = null
    @SerializedName("study_assent")
    @Expose
    private var studyAssent: Boolean? = null
    @SerializedName("additional_user_details")
    @Expose
    private var additionalUserDetails: Boolean? = null
    @SerializedName("study_id")
    @Expose
    private var studyId: Int? = null
    @SerializedName("created_at")
    @Expose
    private var createdAt: String? = null
    @SerializedName("updated_at")
    @Expose
    private var updatedAt: String? = null

    fun getId(): Int? {
        return id
    }

    fun setId(id: Int?) {
        this.id = id
    }

    fun getStudyConsent(): Boolean? {
        return studyConsent
    }

    fun setStudyConsent(studyConsent: Boolean?) {
        this.studyConsent = studyConsent
    }

    fun getStudyAssent(): Boolean? {
        return studyAssent
    }

    fun setStudyAssent(studyAssent: Boolean?) {
        this.studyAssent = studyAssent
    }

    fun getAdditionalUserDetails(): Boolean? {
        return additionalUserDetails
    }

    fun setAdditionalUserDetails(additionalUserDetails: Boolean?) {
        this.additionalUserDetails = additionalUserDetails
    }

    fun getStudyId(): Int? {
        return studyId
    }

    fun setStudyId(studyId: Int?) {
        this.studyId = studyId
    }

    fun getCreatedAt(): String? {
        return createdAt
    }

    fun setCreatedAt(createdAt: String?) {
        this.createdAt = createdAt
    }

    fun getUpdatedAt(): String? {
        return updatedAt
    }

    fun setUpdatedAt(updatedAt: String?) {
        this.updatedAt = updatedAt
    }

}