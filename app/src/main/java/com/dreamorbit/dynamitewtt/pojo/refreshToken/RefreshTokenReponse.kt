package com.dreamorbit.dynamitewtt.pojo.refreshToken

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class RefreshTokenReponse {

    @SerializedName("access_token")
    @Expose
    private var accessToken: String? = null
    @SerializedName("expires_in")
    @Expose
    private var expiresIn: Int? = null
    @SerializedName("token_type")
    @Expose
    private var tokenType: String? = null
    @SerializedName("refresh_token")
    @Expose
    private var refreshToken: String? = null
    @SerializedName("id_token")
    @Expose
    private var idToken: String? = null
    @SerializedName("new_device_metadata")
    @Expose
    private var newDeviceMetadata: Any? = null

    fun getAccessToken(): String? {
        return accessToken
    }

    fun setAccessToken(accessToken: String?) {
        this.accessToken = accessToken
    }

    fun getExpiresIn(): Int? {
        return expiresIn
    }

    fun setExpiresIn(expiresIn: Int?) {
        this.expiresIn = expiresIn
    }

    fun getTokenType(): String? {
        return tokenType
    }

    fun setTokenType(tokenType: String?) {
        this.tokenType = tokenType
    }

    fun getRefreshToken(): String? {
        return refreshToken
    }

    fun setRefreshToken(refreshToken: String?) {
        this.refreshToken = refreshToken
    }

    fun getIdToken(): String? {
        return idToken
    }

    fun setIdToken(idToken: String?) {
        this.idToken = idToken
    }

    fun getNewDeviceMetadata(): Any? {
        return newDeviceMetadata
    }

    fun setNewDeviceMetadata(newDeviceMetadata: Any?) {
        this.newDeviceMetadata = newDeviceMetadata
    }


}