package com.dreamorbit.dynamitewtt.service

import android.util.Log
import com.dreamorbit.dynamitewtt.api.PhPublicKey
import com.dreamorbit.dynamitewtt.application.AppConstants
import com.dreamorbit.dynamitewtt.application.PhAwareApplication
import com.dreamorbit.dynamitewtt.database.registration.RegistartionRepository
import com.google.android.gms.gcm.GcmNetworkManager
import com.google.android.gms.gcm.GcmTaskService
import com.google.android.gms.gcm.PeriodicTask
import com.google.android.gms.gcm.TaskParams
import io.reactivex.SingleObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class PhGCMTaskService : GcmTaskService() {
    private val TAG = "MyTaskService"

    private val MINUTES = 30L
    private val SECONDS = 60L
    fun startPeriodicTask() {
        Log.d("Utility", "startPeriodicTask")
        val mGcmNetworkManager =
            GcmNetworkManager.getInstance(PhAwareApplication())
        // [START start_periodic_task]
        val task =
            PeriodicTask.Builder()
                .setService(PhGCMTaskService::class.java)
                .setTag(AppConstants().TASK_TAG_PERIODIC)
                .setPeriod(MINUTES * SECONDS)
                .build()
        mGcmNetworkManager.schedule(task)
    }

    override fun onInitializeTasks() {}

    override fun onRunTask(taskParams: TaskParams): Int {
        Log.d(TAG, "onRunTask: " + taskParams.tag)
        val tag = taskParams.tag
        // Default result is success.
        val result = GcmNetworkManager.RESULT_SUCCESS
        // Choose method based on the tag.
        if (AppConstants().TASK_TAG_PERIODIC.equals(tag)) {
            doPeriodicTask()
        }
        // Return one of RESULT_SUCCESS, RESULT_FAILURE, or RESULT_RESCHEDULE
        return result
    }

    /**
     * Fetch the registered email from preference and generate sha-256.
     */
    private fun doPeriodicTask() {
        val email: String? =
            RegistartionRepository().getInstance()!!.getRegistrationEmailSha() // Take value from table
        RefreshTokenApi().refreshToken(email)
        //api call to get Public key to encrypt the zip file (SurveyResponse.json)
        downloadCertificate()
    }
    fun downloadCertificate() {
        val publicKeyApi = PublicKeyApi()
        publicKeyApi.getPresendUrl()!!.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : SingleObserver<PhPublicKey?> {
                override fun onSubscribe(d: Disposable) {}
                 override fun onSuccess(phPublicKey: PhPublicKey) {
                        Log.e("Response:", "" + phPublicKey)
                        publicKeyApi.getPublicKey(phPublicKey.getUrl())!!.subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(object : SingleObserver<String?> {
                                override fun onSubscribe(disposable: Disposable) {}
                                override fun onError(throwable: Throwable) {}
                                override fun onSuccess(t: String) {

                                }
                            })
                    }

                override fun onError(e: Throwable) {}

            })
    }
}