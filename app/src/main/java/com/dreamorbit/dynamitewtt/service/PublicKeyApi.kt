package com.dreamorbit.dynamitewtt.service

import com.dreamorbit.dynamitewtt.api.ApiClient
import com.dreamorbit.dynamitewtt.api.IApiRepo
import com.dreamorbit.dynamitewtt.api.PhPublicKey
import io.reactivex.Single
import retrofit2.Retrofit

class PublicKeyApi {
    val apiClient = ApiClient()
    fun getPresendUrl(): Single<PhPublicKey?>? {
        val retrofit: Retrofit = apiClient.getClient("")
        val service: IApiRepo = retrofit.create<IApiRepo>(IApiRepo::class.java)
        return service.getPublicKey()
    }

    fun getPublicKey(awsURL: String?): Single<String?>? { //AWS Client without any Token or Authorization headers
        val retrofitAws: Retrofit = apiClient.getAwsCertificateClient()
        val serviceAws: IApiRepo = retrofitAws.create<IApiRepo>(IApiRepo::class.java)
        return serviceAws.getAwsPublicKey(awsURL)
    }
}