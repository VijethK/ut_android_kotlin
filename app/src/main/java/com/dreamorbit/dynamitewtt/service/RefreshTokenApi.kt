package com.dreamorbit.dynamitewtt.service

import android.util.Log
import com.dreamorbit.dynamitewtt.api.ApiClient
import com.dreamorbit.dynamitewtt.api.IApiRepo
import com.dreamorbit.dynamitewtt.pojo.refreshToken.RefreshTokenReponse
import com.dreamorbit.dynamitewtt.utilities.SharedPrefSingleton
import com.dreamorbit.dynamitewtt.utilities.Utility
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit

class RefreshTokenApi {

    /**
     * in Authorization header for this api send refresh token instead of access token
     */
    fun refreshToken(email: String?) {
        val refreshToken: String =
            SharedPrefSingleton.getInstance().getString(SharedPrefSingleton.REFRESH_TOKEN, "")
        val apiClient = ApiClient()
        val retrofit: Retrofit? = apiClient.getClient(refreshToken)
        val service: IApiRepo = retrofit!!.create<IApiRepo>(IApiRepo::class.java)
        val call: Call<RefreshTokenReponse> = service.refreshToken(email)
        call.enqueue(object : Callback<RefreshTokenReponse> {
            override fun onResponse(
                call: Call<RefreshTokenReponse>,
                response: Response<RefreshTokenReponse>
            ) {
                val utility = Utility()
                if (response != null && response.code() == 200 || response.code() == 201) {
                    Log.e("Token Refreshed:", "" + response.body()!!.getAccessToken())
                    SharedPrefSingleton.getInstance().saveString(
                        SharedPrefSingleton.ACCESS_TOKEN,
                        response.body()!!.getAccessToken()
                    ) //Store the refresh token in Preferences
                    //utility.uploadPendingData()
                } else if (response != null && response.code() == 401 || response.code() == 404) { //unauthorised || not found
                   // Utility.signOut()
                   // Utility.clearAlarms()
                }
            }

            override fun onFailure(
                call: Call<RefreshTokenReponse>,
                t: Throwable
            ) {
                Log.e("", "" + t.toString())
            }
        })
    }
}