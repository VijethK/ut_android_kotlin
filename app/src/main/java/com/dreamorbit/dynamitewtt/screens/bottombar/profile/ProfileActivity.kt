package com.dreamorbit.dynamitewtt.screens.bottombar.profile

import android.os.Bundle
import android.widget.LinearLayout
import android.widget.Switch
import android.widget.TextView
import androidx.annotation.Nullable
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.dreamorbit.dynamitewtt.R

class ProfileActivity : AppCompatActivity(){

    //private val additionalInfo: AdditionalInfo? = null
    private val profile = false
    private val encrypted = "***"
    //private val profilePresenter: ProfilePresenter? = null
    private var androidWatch: Switch? = null
    private var fitbit: Switch? = null
    private val mParent: LinearLayout? = null
    override fun onCreate(@Nullable savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
        //profilePresenter = ProfilePresenter(this, SignOutApi(), WithdrawApi(), GetProfileApi())
        initToolBar()
        initViews()
    }
    private fun initToolBar() {
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        val title: TextView = toolbar.findViewById(R.id.title)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        title.setText(R.string.menu_bottom_about_me)
    }
    private fun initViews() {
        //getting data from AdditionalInfoRepository and assign it to views
        //additionalInfo = AdditionalInfoRepository.getInstance().getAdditionalInfo()
        val age: TextView = findViewById(R.id.tvAge)
        val gender: TextView = findViewById(R.id.tvGender)
        val height: TextView = findViewById(R.id.tvHeight)
        val weight: TextView = findViewById(R.id.tvWeight)
        val diagnoised: TextView = findViewById(R.id.tvDiagnosed)
        val medicated: TextView = findViewById(R.id.tvMedication)
        androidWatch = findViewById(R.id.watch_switch)
        fitbit = findViewById(R.id.fitbit_switch)

       /* age.text = if (profile) StudyRepository.getInstance().getStudyData().getAge() else encrypted
        gender.text = if (profile) additionalInfo.getGender() else encrypted
        height.text = if (profile) additionalInfo.getHeight() else encrypted
        weight.text = if (profile) additionalInfo.getWeigh() else encrypted
        diagnoised.text = if (profile) additionalInfo.getDiagnosed() else encrypted
        medicated.text = if (profile) additionalInfo.getMedication() else encrypted

        androidWatch.setClickable(false)
        fitbit.setClickable(false)
        setWearble(additionalInfo)*/

    }


}