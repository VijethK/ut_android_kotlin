package com.dreamorbit.dynamitewtt.screens.registration

import android.content.DialogInterface
import android.os.Build
import android.os.Bundle
import android.text.TextUtils
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.NonNull
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.Toolbar
import com.dreamorbit.dynamitewtt.R
import com.dreamorbit.dynamitewtt.application.AppConstants
import com.dreamorbit.dynamitewtt.application.PhAwareApplication
import com.dreamorbit.dynamitewtt.base.BaseActivity
import com.dreamorbit.dynamitewtt.database.joinstudy.StudyData
import com.dreamorbit.dynamitewtt.database.registration.RegistartionData
import com.dreamorbit.dynamitewtt.network.ConnectivityReceiver
import com.dreamorbit.dynamitewtt.pojo.register.RegisterRequest
import com.dreamorbit.dynamitewtt.pojo.register.RegisterResponse
import com.dreamorbit.dynamitewtt.service.PhGCMTaskService
import com.dreamorbit.dynamitewtt.service.PublicKeyApi
import com.dreamorbit.dynamitewtt.utilities.RSAEncryption
import com.dreamorbit.dynamitewtt.utilities.SharedPrefSingleton
import com.dreamorbit.dynamitewtt.utilities.Utility
import com.google.android.material.snackbar.Snackbar
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class RegisterActivity : BaseActivity(), View.OnClickListener, RegisterViewContract,
    ConnectivityReceiver.ConnectivityReceiverListener {
    private var mEtEmail: EditText? = null
    private var mEtUserName: EditText? = null
    private var mEtPassword: EditText? = null
    private var mRegisterPresenter: RegisterPresenter? = null
    private var mStudyDataLoaded: StudyData? = null
    private var mRegistartionData: RegistartionData? = null
    val utility = Utility()

    private var mEmailSHA: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        mRegisterPresenter = RegisterPresenter(this, RegisterApi(), PublicKeyApi())

        //toolbar_consent initialization
        initToolbar()

        //views initialization
        initViews()

        //register broadcast receiver for connectivity check for OS >= Nougat
        utility.registerReciverForN(this)
    }

    /**
     * views initialization
     */
    private fun initViews() {
        mEtEmail = findViewById(R.id.email)
        mEtUserName = findViewById(R.id.username)
        mEtPassword = findViewById(R.id.password)
        mRegisterPresenter!!.getStudyDetail()
       mRegistartionData = RegistartionData()
    }

    /*
     *toolbar_consent initialization
     */
    private fun initToolbar() {
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        val done: TextView = toolbar.findViewById(R.id.done)
        done.setOnClickListener(this)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
    }
    /**
     * Onclick listeners
     */
    override fun onClick(view: View) {
        when (view.id) {
            R.id.done -> mRegisterPresenter!!.validation(
                utility.isValidEmail(mEtEmail!!.text.toString().trim { it <= ' ' }),
                mEtUserName!!.text.toString().trim { it <= ' ' },
                mEtPassword!!.text.toString().trim { it <= ' ' }
            )
        }
    }


    /**
     * on menu item click listeners
     */
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    /**
     * Showing validation errors.
     *
     * @param errors
     */
    override fun setValidationErrors(errors: String?) {
        if (errors.equals(getString(R.string.email), ignoreCase = true)) {
            mEtEmail!!.error = getString(R.string.valid_email)
        } else if (errors.equals(getString(R.string.user_name), ignoreCase = true)) {
            mEtUserName!!.error = getString(R.string.valid_username)
        } else if (errors.equals(getString(R.string.password), ignoreCase = true)) {
            mEtPassword!!.error = getString(R.string.valid_password)
        }
    }

    /**
     * Web request for registration.
     */
    @RequiresApi(Build.VERSION_CODES.KITKAT)
    override fun registerWebRequest() {
        utility.hideKeyboard(findViewById<LinearLayout>(R.id.register_layout))
        val publicKey: String = SharedPrefSingleton.getInstance().getPublicKey()
        if (ConnectivityReceiver().isConnected()) {
                    if (TextUtils.isEmpty(publicKey)) {
                        utility.showProgressBar(
                    this@RegisterActivity,
                    getString(R.string.downloading_certificate)
                )
                mRegisterPresenter!!.downloadCertificate()
            } else {
                val register: RegisterRequest = createRegisterRequest()
                        utility.showProgressBar(this@RegisterActivity, getString(R.string.registering))
                mRegisterPresenter!!.registerService(register)
            }
        } else {
            utility.showSnackBar(
                findViewById(R.id.register_layout),
                getString(R.string.check_internet_connection),
                Snackbar.LENGTH_LONG
            )
        }
    }
    @RequiresApi(Build.VERSION_CODES.KITKAT)
    override fun onCertificateDownloadSuccess() {
        utility.dismissProgressBar()
        utility.showProgressBar(this@RegisterActivity, getString(R.string.registering))
        val register: RegisterRequest = createRegisterRequest()
        mRegisterPresenter!!.registerService(register)
    }
    /**
     * Creating the registration model.
     * request param mEtEmail should be in sha-256 format string appended with @phaware.org
     *
     * DOB format yyyy-mm-dd
     * Role will be hardcoded i.e "parrticipant"
     */
    @RequiresApi(Build.VERSION_CODES.KITKAT)
    @NonNull
    private fun createRegisterRequest(): RegisterRequest { //Encrypting the mEtEmail to SHA-256
        mEmailSHA = utility.getSHA256Hash(mEtEmail!!.text.toString().trim { it <= ' ' })
        val register = RegisterRequest()
        register.setEmail(StringBuilder().append(mEmailSHA).append("@phaware.org").toString())
        register.setPassword(mEtPassword!!.text.toString().trim { it <= ' ' })
        register.setName(mEtUserName!!.text.toString().trim { it <= ' ' })
        register.setRole("participant")
        register.setDevice_token(SharedPrefSingleton.getInstance().fcmToken)
        register.setDevice_type(AppConstants().DEVICE_TYPE)
        try {
            register.setActual_email(RSAEncryption().encryptText(mEtEmail!!.text.toString().trim { it <= ' ' }))
            val dateFormat: DateFormat = SimpleDateFormat("yyyy/MM/dd")
            val cal = Calendar.getInstance()
            //Assign present day to end Date
            register.setSurvey_end_date(dateFormat.format(cal.time))
            //Assign yesterday day to start Date
            cal.add(Calendar.DATE, -1)
            register.setSurvey_start_date(dateFormat.format(cal.time))
        } catch (e: Exception) {
            e.printStackTrace()
        }
        //To store in db
        mRegistartionData!!.setEmail(mEtEmail!!.text.toString().trim { it <= ' ' })
        mRegistartionData!!.setEmail_sha(StringBuilder().append(mEmailSHA).append("@phaware.org").toString().trim { it <= ' ' })
        mRegistartionData!!.setUserName(mEtUserName!!.text.toString().trim { it <= ' ' })
        return register
    }


    /**
     * Initilize the internet broadcast listener
     */
    override fun onResume() {
        super.onResume()
        // register connection status listener
        PhAwareApplication().getInstance()!!.setConnectivityListener(this)
    }

    /***
     * If error response during api call show the message in SnackBar
     * @param registerResponse
     */
    override fun onRegistrationFailure(registerResponse: RegisterResponse?) {
        utility.dismissProgressBar()
        utility.showSnackBar(
            findViewById(R.id.register_layout),
            registerResponse!!.getErrorMessage(),
            Snackbar.LENGTH_LONG
        )
    }

    /***
     * If success response during register api call. Store the access token in sared preference
     * @param registerResponse
     *
     * On Success response start periodic task to get refresh token in every 1 hour
     * Navigate to the assent flow
     */

    override fun onRegistrationSuccess(registerResponse: RegisterResponse?): Unit {
        utility.dismissProgressBar()
        if (registerResponse != null) {
            mRegisterPresenter!!.saveRegistartionDetails(mRegistartionData)
            utility.showSnackBar(
                findViewById(R.id.register_layout),
                "Successfully Registered",
                Snackbar.LENGTH_LONG
            )
            SharedPrefSingleton.getInstance()
                .saveString(SharedPrefSingleton.ACCESS_TOKEN, registerResponse.getAccessToken())
            SharedPrefSingleton.getInstance()
                .saveString(SharedPrefSingleton.REFRESH_TOKEN, registerResponse.getRefreshToken())
            SharedPrefSingleton.getInstance()
                .saveUserEmail(java.lang.StringBuilder().append(mEmailSHA).append("@phaware.org").toString().trim { it <= ' ' })
            //optimize this. May be not required
/* Intent intent = new Intent(PhAwareApplication.getMyAppContext(), BackGroundService.class);
            intent.putExtra(AppConstants.BACKGROUND_ACTION, String.valueOf(Utility.BackgroundAction.upload_survey));
            BackGroundService.enqueueBackgroundWork(PhAwareApplication.getMyAppContext(), intent);*/
            //PhGCMTaskService().startPeriodicTask()
            studyConfigNavigation()
        } else {
            utility.showSnackBar(
                findViewById(R.id.register_layout),
                getString(R.string.try_again),
                Snackbar.LENGTH_LONG
            )
        }
    }

    /**
     * If This user is partially registered earlier then navigate him to login screen
     *
     * @param response
     */
    override fun partialRegistration(response: RegisterResponse?) {
        utility.dismissProgressBar()
        showPartialAlertDialog()
    }

    /***
     * Query the study data from database and result will be store in this callback
     * @param studyData
     */
    override fun onStudyDataLoaded(studyData: StudyData?) {
        mStudyDataLoaded = studyData
    }

    /***
     * If connectivity flickers user will get the message using this receiver
     * @param isConnected
     */
    override fun onNetworkConnectionChanged(isConnected: Boolean) {
        if (!isConnected) {
            utility.dismissProgressBar()
            utility.showSnackBar(
                findViewById(R.id.register_layout),
                if (isConnected) getString(R.string.internet_connected) else getString(R.string.internet_disconnected),
                Snackbar.LENGTH_LONG
            )
        }
    }

    /**
     * On successful registration and user ages is  greatet than 17 navigate to the Consent flow activity
     */
    fun showPartialAlertDialog() {
        fun showDialogOKCancel(
            message: String?,
            okText: String?,
            cancelText: String?,
            okListener: DialogInterface.OnClickListener?
        ) {
            dialog?.dismiss()
            val mBuilder: AlertDialog.Builder = AlertDialog.Builder(this)
            mBuilder.setMessage(message)
            mBuilder.setCancelable(false)
            mBuilder.setPositiveButton(okText, okListener)
            mBuilder.setNegativeButton(cancelText, okListener)
            dialog = mBuilder.create()
            dialog!!.show()
        }
    }
    override fun onPause() {
        super.onPause()
        if (utility.receiver != null) utility.unRegisterReciverForN(this)
    }

}
