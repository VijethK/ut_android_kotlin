package com.dreamorbit.dynamitewtt.screens.bottombar.activities

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.dreamorbit.dynamitewtt.R
import com.dreamorbit.dynamitewtt.screens.bottombar.profile.ProfileActivity
import com.dreamorbit.dynamitewtt.custom.SimpleDividerItemDecoration
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class ActivitiesFragment : AppCompatActivity() {
    //Adapter RecyclerView
    //private val mActivitiesAdapter: ActivitiesAdapter? = null
    private var mActivityRecycler: RecyclerView? = null
   // private val mActivityIdArray: IntArray = TODO()
    //private val mActivityList: List<ActivityList>? = null
    private var mActivityIdClicked = 0
    private var mActivityName: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_bottom_activities)
        val date: TextView = findViewById(R.id.tv_date)
        val dateFormat: DateFormat = SimpleDateFormat("MMMM dd") as DateFormat
        val cal = Calendar.getInstance()
        //Assign present day to end Date
        date.text = getString(R.string.current_date, dateFormat.format(cal.time))
        initRecyclerView()
        initToolbar()
    }

    private fun initToolbar() {
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        val title: TextView = toolbar.findViewById(R.id.title)
        val imgProfile: TextView = toolbar.findViewById(R.id.profile)
        title.setText(R.string.menu_bottom_activities)
        imgProfile.setOnClickListener { view: View? ->
            startActivity(Intent(this, ProfileActivity::class.java))
        }
       setSupportActionBar(toolbar)
    }

    private fun initRecyclerView() {
        mActivityRecycler = findViewById(R.id.activity_recycler)
        mActivityRecycler!!.setLayoutManager(LinearLayoutManager(this))
        mActivityRecycler!!.addItemDecoration(SimpleDividerItemDecoration())
       /* mActivityRecycler!!.addOnItemTouchListener(
            RecyclerTouchListener(this,
                mActivityRecycler, object : IRecyclerItemClickListener() {
                    fun onClick(view: View?, position: Int, activityId: Int) {
                        mActivityIdClicked = activityId
                        mActivityName = mActivityList.get(position).getName()
                        //boolean isCompleted = mActivityList.get(position).getIsActivityAnswered() == 1;
                        val isCompleted: Boolean =
                            SchedulerRepository.getInstance().getActivityStatus(activityId)
                        //Disable button click if already test has taken
                        if (mActivityName.equals("6 Minute Walk Test", ignoreCase = true) && walkTestSchedulerSetup(isCompleted)) {
                            Toast.makeText(this, getString(R.string.test_taken_message), Toast.LENGTH_SHORT).show()
                            return
                        }
                        *//**
                         * dont launch the survey if already completed
                         *//*
                        if (!isCompleted) {
                            // initSurveySetup(activityId)
                        }
                    }

                    fun onLongClick(view: View?, position: Int) {}
                })
        )*/
    }
}