package com.dreamorbit.dynamitewtt.screens.splash

import android.Manifest
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.location.LocationManager
import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import android.provider.Settings
import android.view.View
import android.widget.Button
import android.widget.VideoView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.coordinatorlayout.widget.CoordinatorLayout
import com.dreamorbit.dynamitewtt.R
import com.dreamorbit.dynamitewtt.application.PhAwareApplication
import com.dreamorbit.dynamitewtt.network.ConnectivityReceiver
import com.dreamorbit.dynamitewtt.screens.welcome.WelcomeActivity
import com.dreamorbit.dynamitewtt.utilities.LocationHelper
import com.dreamorbit.dynamitewtt.utilities.PermissionsUtility
import com.dreamorbit.dynamitewtt.utilities.SharedPrefSingleton
import com.dreamorbit.dynamitewtt.utilities.Utility
import com.google.android.material.snackbar.Snackbar


class SplashActivity : AppCompatActivity(), MediaPlayer.OnCompletionListener,
    ConnectivityReceiver.ConnectivityReceiverListener {
    private val TAG: String = SplashActivity::class.java.getSimpleName()
    private val REQUEST_CODE = 11
    private var mLocationManager: LocationManager? = null

    private var mPermissionsUtility: PermissionsUtility? = null

    //video view
    private var mContentView: VideoView? = null
    private var isVideoComplete = false
    val phAwareApplication= PhAwareApplication()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        SharedPrefSingleton.getInstance().initialize(this)
        SharedPrefSingleton.getInstance().saveBoolean(SharedPrefSingleton.IS_USA, true)
        //Fabric.with(this, Crashlytics())
        initVideoView()
        initData()
        checkGPSEnabled()
    }
    private fun initData() {
        mPermissionsUtility = PermissionsUtility
    }
    private  fun initVideoView() {
        mContentView = findViewById(R.id.videoView)
        mContentView!!.setOnCompletionListener(this)
        findViewById<Button>(R.id.ta_btn_skip).setOnClickListener(mSkipClickListener)
        if (!SharedPrefSingleton.getInstance()!!.getBoolean(SharedPrefSingleton.IS_USA, false)) {
            findViewById<Button>(R.id.ta_btn_skip).setVisibility(View.GONE)
        }
    }

    private val mSkipClickListener =
        View.OnClickListener { view: View? ->
            if (SharedPrefSingleton.getInstance()!!.getBoolean(SharedPrefSingleton.IS_USA, false)) {
                isVideoComplete = true
              //  navigationFlow()
                finish()
            } else { //stay in his page unless permission has given
                //requestPermission()
            }
        }
    override fun onResume() {
        super.onResume()
        // register connection status listener
        //Utility.registerReciverForN(this)
//        phAwareApplication.getInstance()!!.setConnectivityListener(this)
    }

    private  fun checkGPSEnabled() {
        mLocationManager =
            getSystemService(Context.LOCATION_SERVICE) as LocationManager
        if (mLocationManager!!.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            requestPermission();
        } else {
            showGPSDisabledAlertToUser()
        }
    }

    private fun showGPSDisabledAlertToUser() {
        val alertDialogBuilder: AlertDialog.Builder = AlertDialog.Builder(this)
        alertDialogBuilder.setMessage(resources.getString(R.string.gps_message))
            .setCancelable(false)
            .setPositiveButton(
                resources.getString(R.string.gps_enable),
                DialogInterface.OnClickListener { dialog, id ->
                    val callGPSSettingIntent = Intent(
                        Settings.ACTION_LOCATION_SOURCE_SETTINGS
                    )
                    startActivityForResult(callGPSSettingIntent, REQUEST_CODE)
                })
        alertDialogBuilder.setNegativeButton(
            resources.getString(R.string.cancel),
            DialogInterface.OnClickListener { dialog, id -> dialog.cancel() })
        alertDialogBuilder.show()
    }

    override fun onPause() {
        super.onPause()
        //if (Utility.receiver != null) unregisterReceiver(Utility.receiver)
    }

     override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String?>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions!!, grantResults)
        when (requestCode) {
            PermissionsUtility.PERMISSIONS_REQUEST -> {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.size > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED
                ) {
                    val locationHelper = LocationHelper()
                    locationHelper.startLocationUpdates()
                } else {
                    requestPermission()
                }
                return
            }
        }
    }

    private fun requestPermission() {
        if (mPermissionsUtility!!.hasPermissions(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            )
        ) {
            val locationHelper = LocationHelper()
            locationHelper.startLocationUpdates()
        } else {
            if (mPermissionsUtility!!.shouldShowRationale(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                )
            ) {
                Snackbar.make(
                    findViewById<CoordinatorLayout>(R.id.main_content),
                    "Location permission is required to fetch location.",
                    Snackbar.LENGTH_INDEFINITE
                ).setAction("OK", View.OnClickListener {
                    // Request the permission
                    mPermissionsUtility!!.requestForLocationPermission(this@SplashActivity)
                }).show()
            } else {
                mPermissionsUtility!!.requestForLocationPermission(this)
            }
        }
    }

    open fun navigationFlow() {
        if (!isVideoComplete) return  // dont go further unless video completes
        gotoWelcome()
        //PhGCMTaskService.startPeriodicTask() // Refresh the Access_Token and Public key
       /* if (BuildConfig.IS_DENVER) {
            if (FlowRepository.getInstance().getAssentStatus(AppConstants.ADDITIONAL_INFO_FLOW)) {
                goToSurveyActivity()
            } else if (FlowRepository.getInstance().getAssentStatus(AppConstants.REGISTER_FLOW)) {
                gotoConsent() // Consent I Screen
            } else if (FlowRepository.getInstance().getAssentStatus(AppConstants.CONSENT_ONE_FLOW)) { //Denver
                goToAssent() // Consent II screen
            } else if (FlowRepository.getInstance().getAssentStatus(AppConstants.CONSENT_TWO_FLOW)) { //Denver
                goToAdditionalInfo()
            } else {
                gotoWelcome()
            }
        } else { //Stanford
            if (FlowRepository.getInstance().getAssentStatus(AppConstants.ADDITIONAL_INFO_FLOW)) { // If Stanford use old flow after registration
                goToSurveyActivity()
            } else {
                gotoWelcome()
            }
       }*/
    }

    override fun onActivityResult(
        requestCode: Int,
        resultCode: Int,
        data: Intent?
    ) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            REQUEST_CODE -> {
                checkGPSEnabled()
            }
        }
    }
    open fun gotoWelcome() {
        startActivity(Intent(this@SplashActivity, WelcomeActivity::class.java))
        finish()
    }


    /*
    private open fun gotoConsent() {
        startActivity(Intent(this@SplashActivity, ConsentActivity::class.java))
        finish()
    }

    private open fun goToAssent() {
        startActivity(Intent(this@SplashActivity, AssentActivity::class.java))
        finish()
    }

    private open fun goToAdditionalInfo() {
        startActivity(Intent(this@SplashActivity, AdditionalInfoActivity::class.java))
        finish()
    }

    private open fun goToSurveyActivity() {
        startActivity(Intent(this@SplashActivity, BottomBarActivity::class.java))
        finish()
    }*/

    override open fun onNetworkConnectionChanged(isConnected: Boolean) {
        if (!isConnected) Utility().showSnackBar(
            findViewById<CoordinatorLayout>(R.id.main_content),
            if (isConnected) getString(R.string.internet_connected) else getString(R.string.internet_disconnected),
            Snackbar.LENGTH_SHORT
        )
    }


    override fun onCompletion(mp: MediaPlayer?) {
        isVideoComplete = true
        if (SharedPrefSingleton.getInstance()!!.getBoolean(SharedPrefSingleton.IS_USA, false)) {
            navigationFlow()
            finish()
        } else { //stay in his page unless permission has given
            requestPermission()
        }
    }

    override fun onStart() {
        super.onStart()
        mContentView!!.setVideoURI(Uri.parse("android.resource://" + packageName.toString() + "/" + R.raw.ph_logo))
        mContentView!!.start()
    }
}