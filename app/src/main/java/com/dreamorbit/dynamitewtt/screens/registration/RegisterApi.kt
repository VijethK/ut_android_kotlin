package com.dreamorbit.dynamitewtt.screens.registration

import android.util.Log
import com.dreamorbit.dynamitewtt.api.ApiClient
import com.dreamorbit.dynamitewtt.api.IApiRepo
import com.dreamorbit.dynamitewtt.pojo.register.RegisterRequest
import com.dreamorbit.dynamitewtt.pojo.register.RegisterResponse
import com.google.gson.Gson
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import java.io.IOException

class RegisterApi {
    fun register(
        register: RegisterRequest?,
        registerPresenter: RegisterPresenter
    ) {
        val apiClient = ApiClient()
        val retrofit: Retrofit = apiClient.getClient("")
        val service: IApiRepo = retrofit.create<IApiRepo>(IApiRepo::class.java)
        val call: Call<RegisterResponse> = service.getRegister(register)
        call.enqueue(object : Callback<RegisterResponse> {
            override fun onResponse(
                call: Call<RegisterResponse>,
                response: Response<RegisterResponse>
            ) {
                if (response.code() == 200 || response.code() == 201) {
                    Log.e("Response:", "" + response.body())
                    registerPresenter.registerResult(response.body())
                } else if (response.code() == 409) { // already exist
                    val gson = Gson()
                    try {
                        val errorResponse: RegisterResponse = gson.fromJson<RegisterResponse>(
                            response.errorBody()!!.string(),
                            RegisterResponse::class.java
                        )
                        registerPresenter.OnPartialRegistered(errorResponse)
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }
                } else {
                    try {
                        val gson = Gson()
                        val errorResponse: RegisterResponse = gson.fromJson<RegisterResponse>(
                            response.errorBody()!!.string(),
                            RegisterResponse::class.java
                        )
                        registerPresenter.registerResult(errorResponse)
                    } catch (e: Exception) {
                        val registerResponse = RegisterResponse()
                        registerResponse.setErrorMessage("Please try again.")
                        e.printStackTrace()
                    }
                }
            }

            override fun onFailure(
                call: Call<RegisterResponse>,
                t: Throwable
            ) {
                Log.e("", "" + t.toString())
                val registerResponse = RegisterResponse()
                registerResponse.setErrorMessage("Registration failed. Please try again.")
                registerPresenter.registerResult(registerResponse)
            }
        })
    }


}