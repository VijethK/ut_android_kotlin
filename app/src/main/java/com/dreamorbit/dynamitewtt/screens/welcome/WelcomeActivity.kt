package com.dreamorbit.dynamitewtt.screens.welcome

import android.content.Intent
import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.dreamorbit.dynamitewtt.MainActivity
import com.dreamorbit.dynamitewtt.R
import com.dreamorbit.dynamitewtt.screens.login.LoginActivity
import com.dreamorbit.dynamitewtt.screens.registration.RegisterActivity
import com.dreamorbit.dynamitewtt.utilities.SharedPrefSingleton
import com.rd.PageIndicatorView


class WelcomeActivity : AppCompatActivity(), View.OnClickListener {
    private val TAG = "WelcomeActivity"
    private var mSectionsPagerAdapter: SectionsPagerAdapter? = null
    private var mViewPager: ViewPager? = null
  //  private val mWelcomePresenter: WelcomePresenter? = null
    private var mBtnJoinStudy: Button? = null
    private var mTvAlreadyParticipating: TextView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_welcome)

        //googleFitSetUp()
        //initToolbar()
        initViews()
        initViewPager()

        SharedPrefSingleton.getInstance().initialize(this)
        //Reset the Rating value to 0, that means fresh installation
        SharedPrefSingleton.getInstance().saveRateCount(0)

        //Initializing Presenter
        //mWelcomePresenter = WelcomePresenter(this, this, InviteApi())
    }
    private fun initViews() {
        mBtnJoinStudy = findViewById(R.id.btnJoinStudy)
        mTvAlreadyParticipating = findViewById(R.id.already_participating)
        mBtnJoinStudy!!.setOnClickListener(this)
        mTvAlreadyParticipating!!.setOnClickListener(this)
    }

   override fun onClick(view: View) {
        when (view.id) {
            R.id.btnJoinStudy ->
               //mWelcomePresenter.joinStudy()
                startActivity(Intent(this@WelcomeActivity, RegisterActivity::class.java))
            R.id.already_participating ->
                startActivity(Intent(this@WelcomeActivity, LoginActivity::class.java))
        }
    }

    private fun initViewPager() {
        mSectionsPagerAdapter = SectionsPagerAdapter()
        mViewPager = findViewById(R.id.container)
        mViewPager!!.setAdapter(mSectionsPagerAdapter)
        val pageIndicatorView: PageIndicatorView = findViewById(R.id.pageIndicatorView)
        pageIndicatorView.setViewPager(mViewPager)
    }

    class SectionsPagerAdapter : PagerAdapter() {
        override fun isViewFromObject(view: View, `object`: Any): Boolean {
            return  `object`== view
        }

        override fun getCount(): Int {
            return 5;
        }

        override fun instantiateItem(collection: ViewGroup, position: Int): Any {
            val inflater = LayoutInflater.from(collection.context)
            val layout =
                inflater.inflate(R.layout.welcome_to_walk_talk, collection, false) as ViewGroup
            val title = layout.findViewById<TextView>(R.id.title)
            val description = layout.findViewById<TextView>(R.id.descrption)
            val swipeMore = layout.findViewById<TextView>(R.id.swipeMore)
            description.movementMethod = ScrollingMovementMethod()
            when (position) {
                0 -> {
                    title.setText(collection.context.resources.getString(R.string.welcome_to_walktalktrack))
                    description.setText(collection.context.resources.getString(R.string.welcome_to_walktalktrack_desc))
                }
                1 -> {
                    swipeMore.visibility = View.INVISIBLE
                    title.setText(collection.context.resources.getString(R.string.how_this_study_works))
                    description.setText(collection.context.resources.getString(R.string.how_this_study_works_desc))
                }
                2 -> {
                    swipeMore.visibility = View.INVISIBLE
                    title.setText(collection.context.resources.getString(R.string.what_is_walktalktrack))
                    description.setText(collection.context.resources.getString(R.string.what_is_walktalktrack_desc))
                }
                3 -> {
                    swipeMore.visibility = View.INVISIBLE
                    title.setText(collection.context.resources.getString(R.string.who_can_participate))
                    description.setText(collection.context.resources.getString(R.string.who_can_participate_desc))
                }
                4 -> {
                    swipeMore.visibility = View.INVISIBLE
                    title.setText(collection.context.resources.getString(R.string.who_we_are))
                    description.setText(collection.context.resources.getString(R.string.who_we_are_desc))
                }
            }
            collection.addView(layout)
            return layout
        }

        override fun destroyItem(collection: ViewGroup, position: Int, `object`: Any) {
            collection.removeView(`object` as View)
        }

    }

}

