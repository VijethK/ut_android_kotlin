package com.dreamorbit.dynamitewtt.screens.login

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.dreamorbit.dynamitewtt.R
import com.dreamorbit.dynamitewtt.screens.bottombar.activities.ActivitiesFragment

class LoginActivity : AppCompatActivity() {
    private var email: EditText? = null
    private var password: EditText? = null
   // private val loginPresenter: LoginPresenter? = null
   // private val mRegistartionData: RegistartionData? = null
    private val emailSHA: String? = null
    private var parent: LinearLayout? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        //loginPresenter = LoginPresenter(this, LoginApi())
        initToolbar()
        initViews()
    }

    private fun initToolbar() {
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        val title: TextView = toolbar.findViewById(R.id.title)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        title.setText(R.string.sign_in)
    }
    private fun initViews() {
        email = findViewById(R.id.et_email)
        password = findViewById(R.id.et_password)
        //mRegistartionData = RegistartionData()
        parent = findViewById(R.id.login_parent)
    }
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_main, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.action_done -> {
                startActivity(Intent(this, ActivitiesFragment::class.java))

                /*loginPresenter.validateSignIn(tility.isValidEmail(email!!.text.toString().trim { it <= ' ' }),
                    password!!.text.toString().trim { it <= ' ' })*/
                //Utility.hideKeyboard(email)
              //  finish()
            }
            android.R.id.home -> { finish()
                }

        }
        return super.onOptionsItemSelected(item)
    }
}
