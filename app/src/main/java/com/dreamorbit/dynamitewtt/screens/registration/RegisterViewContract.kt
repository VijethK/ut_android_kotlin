package com.dreamorbit.dynamitewtt.screens.registration

import com.dreamorbit.dynamitewtt.database.joinstudy.StudyData
import com.dreamorbit.dynamitewtt.pojo.register.RegisterResponse

interface RegisterViewContract {
    fun onRegistrationFailure(registerResponse: RegisterResponse?)

    fun onRegistrationSuccess(registerResponse: RegisterResponse?)

    fun onStudyDataLoaded(studyData: StudyData?)

    fun setValidationErrors(errors: String?)

    fun registerWebRequest()

    fun partialRegistration(response: RegisterResponse?)

    fun onCertificateDownloadSuccess()
}