package com.dreamorbit.dynamitewtt.screens.registration

import android.util.Log
import com.dreamorbit.dynamitewtt.api.PhPublicKey
import com.dreamorbit.dynamitewtt.database.joinstudy.StudyRepository
import com.dreamorbit.dynamitewtt.database.registration.RegistartionData
import com.dreamorbit.dynamitewtt.database.registration.RegistartionRepository
import com.dreamorbit.dynamitewtt.pojo.register.RegisterRequest
import com.dreamorbit.dynamitewtt.pojo.register.RegisterResponse
import com.dreamorbit.dynamitewtt.service.PublicKeyApi
import com.dreamorbit.dynamitewtt.utilities.SharedPrefSingleton
import io.reactivex.SingleObserver
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class RegisterPresenter(
    registerActivity: RegisterActivity,
    registerApi: RegisterApi,
    publicKeyApi: PublicKeyApi
) {
    private var mRegisterView: RegisterViewContract? = registerActivity
    private var mRegisterApi: RegisterApi? = registerApi
    private var publicKeyApi: PublicKeyApi? = publicKeyApi


    /**
     * fetch the data from database using repository class
     */
    fun getStudyDetail() {
        mRegisterView!!.onStudyDataLoaded(StudyRepository().getInstance()!!.getStudyData())
    }

    /**
     * On successfull registration. persist the register data in sqlite for future use
     */
    fun saveRegistartionDetails(registartionData: RegistartionData?) {
        RegistartionRepository().getInstance()!!.deleteRegistrationData()
        RegistartionRepository().getInstance()!!.saveRegistrationData(registartionData)
    }

    /**
     * Call the api to register the user using service layer object
     */
    fun registerService(registerRequest: RegisterRequest?) {
        mRegisterApi!!.register(registerRequest, this)
    }
    fun downloadCertificate() {
        publicKeyApi!!.getPresendUrl()!!.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : SingleObserver<PhPublicKey?> {
                 override fun onSubscribe(d: Disposable) {}
                override fun onSuccess(phPublicKey: PhPublicKey) {
                    if (phPublicKey != null) {
                        Log.e("Response:", "" + phPublicKey)
                        publicKeyApi!!.getPublicKey(phPublicKey.getUrl())!!.subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(object : SingleObserver<String?> {
                                override fun onSubscribe(disposable: Disposable) {}
                                override fun onSuccess(s: String) {
                                    mRegisterView!!.onCertificateDownloadSuccess()
                                }

                                override fun onError(throwable: Throwable) {
                                    mRegisterView!!.onCertificateDownloadSuccess()
                                }
                            })
                    }
                }

                override fun onError(e: Throwable) {

                }

            })
    }
    /**
     * Validation of registration fields
     *
     * @param email
     * @param userName
     * @param password
     */
    fun validation(
        email: Boolean,
        userName: String,
        password: String
    ) {
        if (!email) {
            mRegisterView!!.setValidationErrors("email")
        } else if (userName.trim { it <= ' ' }.isEmpty()) {
            mRegisterView!!.setValidationErrors("userName")
        } else if (password.trim { it <= ' ' }.isEmpty()) {
            mRegisterView!!.setValidationErrors("password")
        } else {
            mRegisterView!!.registerWebRequest()
        }
    }

    /**
     * Registration service result
     *
     * @param registerResponse
     */
    fun registerResult(registerResponse: RegisterResponse?) {
        if (registerResponse?.getAccessToken() != null) {
            saveStudyConfiguration(registerResponse)
            mRegisterView!!.onRegistrationSuccess(registerResponse)
        } else if (registerResponse != null) {
            mRegisterView!!.onRegistrationFailure(registerResponse)
        }
    }

    /***
     * if response.code() == 409 that means partially registered
     * @param registerResponse
     */
    fun OnPartialRegistered(registerResponse: RegisterResponse) {
        saveStudyConfiguration(registerResponse)
        mRegisterView!!.partialRegistration(registerResponse)
    }

    private fun saveStudyConfiguration(regResponse: RegisterResponse) {
        SharedPrefSingleton.getInstance().saveStudyConfig(regResponse.getStudyConfiguration())
    }
}