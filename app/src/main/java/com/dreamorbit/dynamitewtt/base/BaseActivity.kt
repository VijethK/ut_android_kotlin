package com.dreamorbit.dynamitewtt.base

import android.Manifest
import android.app.ProgressDialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.annotation.NonNull
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.coordinatorlayout.widget.CoordinatorLayout
import com.dreamorbit.dynamitewtt.R
import com.dreamorbit.dynamitewtt.utilities.PermissionsUtility
import com.google.android.material.snackbar.Snackbar
import java.io.IOException
import java.nio.charset.StandardCharsets
import androidx.appcompat.app.AlertDialog
import com.dreamorbit.dynamitewtt.application.AppConstants
import com.dreamorbit.dynamitewtt.database.flow.FlowRepository
import com.dreamorbit.dynamitewtt.network.ConnectivityReceiver
import com.dreamorbit.dynamitewtt.pojo.register.StudyConfiguration
import com.dreamorbit.dynamitewtt.screens.login.LoginActivity
import com.dreamorbit.dynamitewtt.screens.registration.RegisterActivity
import com.dreamorbit.dynamitewtt.screens.welcome.WelcomeActivity
import com.dreamorbit.dynamitewtt.utilities.SharedPrefSingleton
import com.dreamorbit.dynamitewtt.utilities.Utility
import io.reactivex.android.BuildConfig


open class BaseActivity : AppCompatActivity() {
    /*Wear*/
    protected val START_ACTIVITY_PATH = "/start-activity"
    protected val STOP_ACTIVITY_PATH = "/stop-activity"
    var dialog: AlertDialog? = null
    var mProgressloader: ProgressDialog? = null
    val appConstants = AppConstants()
    private var mPermissionsUtility: PermissionsUtility? = null

    override fun onRestart() { //overridePendingTransition(R.animator.slide_from_left, R.animator.slide_to_right);
        super.onRestart()
    }

    override fun startActivity(intent: Intent?) {
        super.startActivity(intent)
        overridePendingTransition(R.animator.slide_from_right, R.animator.slide_to_left)
    }

    override fun startActivityForResult(intent: Intent?, requestCode: Int) {
        super.startActivityForResult(intent, requestCode)
        overridePendingTransition(R.animator.slide_from_right, R.animator.slide_to_left)
    }

    override fun onStart() {
        super.onStart()
    }

    override fun onStop() {
        super.onStop()
    }
    override fun onResume() {
        super.onResume()
    }

    fun showLoader(message: String?) {
        runOnUiThread(Runnable {
            if (mProgressloader != null || isFinishing) {
                return@Runnable
            }
            hideKeyboard()
            mProgressloader = ProgressDialog(this@BaseActivity)
            mProgressloader!!.window!!.setBackgroundDrawable(ColorDrawable(Color.LTGRAY))
            mProgressloader!!.setProgressStyle(ProgressDialog.STYLE_SPINNER)
            mProgressloader!!.setCancelable(false)
            mProgressloader!!.isIndeterminate = true
            mProgressloader!!.setMessage(message)
            try {
                mProgressloader!!.show()
            } catch (e: WindowManager.BadTokenException) {
                mProgressloader = null
            } catch (e: Exception) {
                mProgressloader = null
            }
        })
    }

    fun hideLoader() {
        runOnUiThread {
            if (mProgressloader != null && mProgressloader!!.isShowing) {
                try {
                    mProgressloader!!.dismiss()
                } catch (e: WindowManager.BadTokenException) {
                    mProgressloader = null
                } catch (e: java.lang.Exception) {
                    mProgressloader = null
                }
            }
            mProgressloader = null
        }
    }
    private fun requestCallPermission() {
        mPermissionsUtility = PermissionsUtility

        if (mPermissionsUtility!!.hasPermissions(this, Manifest.permission.CALL_PHONE)) {
        } else {
            if (mPermissionsUtility!!.shouldShowRationale(this, Manifest.permission.CALL_PHONE)) {

                Snackbar.make(
                    findViewById<CoordinatorLayout>(R.id.main_content),
                    "Call permission is required for emergency call",
                    Snackbar.LENGTH_INDEFINITE
                ).setAction("OK", View.OnClickListener {
                    // Request the permission
                    mPermissionsUtility!!.requestForCallPermission(this)
                }).show()
            } else {
                mPermissionsUtility!!.requestForCallPermission(this)
            }
        }
    }

    /**
     * Phone Call Permission check for triggering emergency call
     *
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    override fun onRequestPermissionsResult(requestCode: Int, @NonNull permissions: Array<String?>, @NonNull grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions!!, grantResults)
        if (requestCode == PermissionsUtility.PERMISSIONS_REQUEST) {
            for (result in grantResults) {
                if (result == PackageManager.PERMISSION_GRANTED) {
                  //  ResearchUtility.triggerCall(this)
                } else {
                    requestCallPermission()
                }
            }
        }
    }
    /***********************************************
     * Show alert dialog with Ok and Cancel
     * @param message
     * @param okListener
     */
    open fun showDialogOKCancel(
        message: String?,
        okText: String?,
        cancelText: String?,
        okListener: DialogInterface.OnClickListener?
    ) {
        dialog?.dismiss()
        val mBuilder: AlertDialog.Builder = AlertDialog.Builder(this)
        mBuilder.setMessage(message)
        mBuilder.setCancelable(false)
        mBuilder.setPositiveButton(okText, okListener)
        mBuilder.setNegativeButton(cancelText, okListener)
        dialog = mBuilder.create()
        dialog?.show()
    }

    /**
     * Show alert dialog with Ok
     *
     * @param message
     * @param okListener
     */
    fun showDialogOK(
        message: String?,
        okText: String?,
        okListener: DialogInterface.OnClickListener?
    ) {
        AlertDialog.Builder(this)
            .setCancelable(false)
            .setMessage(message)
            .setPositiveButton(okText, okListener)
            .setCancelable(false)
            .create()
            .show()
    }

    fun showMsg(msg: String? /*, final View view*/) {
        runOnUiThread { Toast.makeText(this@BaseActivity, msg, Toast.LENGTH_LONG).show() }
    }

    private fun hideKeyboard() {
        val view = currentFocus
        if (view != null) {
            (getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).hideSoftInputFromWindow(
                view.windowToken,
                InputMethodManager.HIDE_NOT_ALWAYS
            )
        }
    }

    fun displayRatingAlert() {
        val connectivityreceiver = ConnectivityReceiver()
        if (!connectivityreceiver!!.isConnected()) return
        val count: Int = SharedPrefSingleton.getInstance().getRateCount()
        //if count is -1 that means already rated
//if count is 0 app has not taken any test yet
//if count is 1, taken 1 test and rating need to show
        if (count == 1) { //How to call the custom dialog sample
            showDialogOK("Do you want to rate this test?", "Rate",
                DialogInterface.OnClickListener { dialog: DialogInterface?, which: Int ->
                    when (which) {
                        DialogInterface.BUTTON_POSITIVE -> showRatingScreen()
                        DialogInterface.BUTTON_NEGATIVE -> {
                        }
                    }
                }
            )
        }
    }

    /**
     * Fetch the assent flow json from asset and pass it it to sdk
     *
     * @return assentJson
     */

    @RequiresApi(Build.VERSION_CODES.KITKAT)
    open fun loadAssentJSONFromAsset(): String? {
        var assentJson: String? = null
        assentJson = try {
            val `is` = assets.open("Assent.json")
            val size = `is`.available()
            val buffer = ByteArray(size)
            `is`.read(buffer)
            `is`.close()
            String(buffer, StandardCharsets.UTF_8)
        } catch (ex: IOException) {
            ex.printStackTrace()
            return null
        }
        return assentJson
    }

    /**
     * Fetch the flow json from asset and pass it it to sdk
     *
     * @return assentJson
     */
    @RequiresApi(Build.VERSION_CODES.KITKAT)
    fun loadConsentJSONFromAsset(): String? {
        var assentJson: String? = null
        assentJson = try {
            val `is` = assets.open("Consent.json")
            val size = `is`.available()
            val buffer = ByteArray(size)
            `is`.read(buffer)
            `is`.close()
            String(buffer, StandardCharsets.UTF_8)
        } catch (ex: IOException) {
            ex.printStackTrace()
            return null
        }
        return assentJson
    }
    /**
     * Showing Rating screen
     */
    protected fun showRatingScreen() {
        //startActivity(Intent(this, RatingActivity::class.java))
    }

    protected fun gotoWelcome() {
        startActivity(Intent(this, WelcomeActivity::class.java))
        finish()
    }

    /*protected fun gotoConsent() {
        val intent = Intent(this, ConsentActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
        finish()
    }

    protected fun goToAssent() {
        val intent = Intent(this, AssentActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
        finish()
    }

    protected fun goToAdditionalInfo() {
        val intent = Intent(this, AdditionalInfoActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
        finish()
    }

    protected fun goToSurveyActivity() {
        val intent = Intent(this, BottomBarActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)
        finish()
    }*/

    protected fun goToRegistrationActivity() {
        val intent = Intent(this, RegisterActivity::class.java)
        startActivity(intent)
        finish()
    }

    protected fun goToLoginActivity() {
        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
        finish()
    }
    /**
     * On login success callback we will call this method to navigate respective screens
     * We will get the status as lastly which api has been successfully called and
     * Which process is completed so far.
     *
     * @param screen
     */
   /* protected fun navigateToPartialRegisteredScreenDenver(screen: Utility.PARTIAL_SCREEN?) {
        val studyConfig: StudyConfiguration = SharedPrefSingleton.getInstance().getStudyConfig()
        when (screen) {
            signup -> {
                FlowRepository.getInstance().saveFlow(appConstants.REGISTER_FLOW, true)
                gotoConsent()
            }
            consent -> if (studyConfig.getStudyConsent()) {
                FlowRepository.getInstance().saveFlow(appConstants.REGISTER_FLOW, true)
                gotoConsent()
            } else if (studyConfig.getStudyAssent()) {
                FlowRepository.getInstance().saveFlow(appConstants.CONSENT_ONE_FLOW, true)
                goToAssent() // Consent I screen once that done show Consent 2 screen
            } else {
                FlowRepository.getInstance().saveFlow(appConstants.CONSENT_TWO_FLOW, true)
                goToAdditionalInfo()
            }
            assent -> if (studyConfig.getStudyAssent()) {
                FlowRepository.getInstance().saveFlow(AppConstants.CONSENT_ONE_FLOW, true)
                goToAssent() // Consent I screen once that done show Consent 2 screen
            } else {
                FlowRepository.getInstance().saveFlow(appConstants.CONSENT_TWO_FLOW, true)
                goToAdditionalInfo()
            }
            user_profile -> {
                FlowRepository.getInstance().saveFlow(appConstants.CONSENT_TWO_FLOW, true)
                goToAdditionalInfo()
            }
            complete -> {
                FlowRepository.getInstance().saveFlow(appConstants.ADDITIONAL_INFO_FLOW, true)
                goToSurveyActivity()
            }
        }
    }*/

    /**
     * We will get the status as lastly which api has been successfully called and
     * Which process is completed so far.
     *
     * @param screen
     */
    /*protected fun navigateToPartialRegisteredScreenStanford(screen: PARTIAL_SCREEN?) {
        when (screen) {
            complete -> {
                FlowRepository.getInstance().saveFlow(appConstants.ADDITIONAL_INFO_FLOW, true)
                goToSurveyActivity()
            }
        }
    }
*/
    /***
     * On onRegistrationSuccess this method will called to navigate to next screen
     */
   protected fun studyConfigNavigation() {
        val studyConfig: StudyConfiguration = SharedPrefSingleton.getInstance().getStudyConfig()
        //Check the build flavour
//If stanford, first show the Assent flow and then Consent as usual old logic and flow
//If Denver show two consent screen as per new requirement from John
       /* if (BuildConfig.IS_DENVER) {
            if (studyConfig.getStudyConsent()!!) {
                FlowRepository().getInstance()!!.saveFlow(appConstants.REGISTER_FLOW, true)
               // gotoConsent()
            } else if (studyConfig.getStudyAssent()!!) {
                FlowRepository().getInstance()!!.saveFlow(appConstants.CONSENT_ONE_FLOW, true)
                //goToAssent() // Consent I screen once that done show Consent 2 screen
            } else {
                FlowRepository().getInstance()!!.saveFlow(appConstants.CONSENT_TWO_FLOW, true)
                //goToAdditionalInfo()
            }
        } else {*/
            FlowRepository().getInstance()!!.saveFlow(appConstants.ADDITIONAL_INFO_FLOW, true)
            //goToSurveyActivity()
       // }
    }

}