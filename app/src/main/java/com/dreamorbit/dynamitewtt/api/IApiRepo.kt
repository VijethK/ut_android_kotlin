package com.dreamorbit.dynamitewtt.api

import com.dreamorbit.dynamitewtt.pojo.additionalInfo.AdditionalInfoRequest
import com.dreamorbit.dynamitewtt.pojo.refreshToken.RefreshTokenReponse
import com.dreamorbit.dynamitewtt.pojo.register.RegisterRequest
import com.dreamorbit.dynamitewtt.pojo.register.RegisterResponse
import io.reactivex.Observable
import io.reactivex.Single
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*

interface IApiRepo {

  /*  @POST("/auth/signin")
    fun signIn(@Body register: SignInRequest?): Call<SignInResponse?>?*/

    @POST("/auth/signup")
    fun getRegister(@Body register: RegisterRequest?): Call<RegisterResponse>

   /* @POST("/study_assents")
    fun createAssent(@Body request: AssentCreateRequest?): Call<AssentCreatedResponse?>?

    @POST("/study_consents")
    fun createConsent(@Body request: ConsentCreateRequest?): Call<ConsentResponse?>?*/

    @FormUrlEncoded
    @POST("/auth/refresh_token")
    fun refreshToken(@Field("email") email: String?): Call<RefreshTokenReponse>

    @POST("/auth/UserProfile")
    fun sendAdditionalInfo(@Body request: AdditionalInfoRequest?): Single<ResponseBody>

    /*@GET("/activities")
    fun getActivities(): Call<Activities?>?

    @GET("/activities/{activity_id}/questions")
    fun getQuestionsList(@Path("activity_id") activityID: Int): Call<QuestionList?>?*/

    @FormUrlEncoded
    @HTTP(method = "DELETE", path = "/auth/signout", hasBody = true)
    fun signOut(@Field("email") email: String?, @Field("device_type") deviceType: String?): Call<Void?>?

    @FormUrlEncoded
    @HTTP(method = "DELETE", path = "/study_consents/withdraw", hasBody = true)
    fun consentWithdraw(@Field("email") email: String?): Call<Void?>?

    /*@GET("/rss")
    fun getPhAwareFeed(): Call<Feed?>?*/

    @GET("/study/certificate.json")
    fun getPublicKey(): Single<PhPublicKey?>?

    @GET
    fun getAwsPublicKey(@Url awsUrl: String?): Single<String?>?

   /* @POST("/uploads")
    fun upload(@Body phUploadRequest: PhUploadRequest?): Observable<PhUploadResponse?>?*/

    @PUT
    fun uploadPresigned(@Url awsUrl: String?, @Body file: RequestBody?, @HeaderMap headers: Map<String?, String?>?): Observable<ResponseBody?>?

    @PUT("/uploads/{id}")
    fun uploadConfirmation(@Path("id") uploadID: Int): Observable<ResponseBody?>?

   /* @GET("/auth/UserProfile")
    fun getUserProfile(): Call<ProfileResponse?>?

    @GET("json")
    fun getGeoLocation(@QueryMap options: Map<String?, Any?>?): Call<GeoResponse?>?

    @GET("weather")
    fun getWeather(@QueryMap options: Map<String?, Any?>?): Call<WeatherResponse?>?

    @POST("/ratings")
    fun ratings(@Body ratingRequest: Rating?): Call<Any?>?

    @POST("/reports")
    fun postReports(@Body reportRequest: PostReportRequest?): Call<Any?>?

    @GET("/reports")
    fun getReports(@QueryMap options: Map<String?, String?>?): Call<ReportResponse?>?

    @PUT("/auth/users/device_info")
    fun updateFcm(@Body fcmPutRequest: FcmPutRequest?): Call<Any?>?
*/
    @GET("/study/verify_token")
    fun validateInviteToken(@Query("user_token") userToken: String?): Call<ResponseBody?>?

   /* @GET("/study/token_verification_required")
    fun validateTokenOnOFF(): Single<InviteToggleResponse?>?

    @GET(" /study_documents/get_latest_doc")
    fun getStudyDocument(@Query("doc_type") studyType: String?): Single<DocumentResponse?>?*/

    @GET
    fun downloadFromAws(@Url fileUrl: String?): Call<ResponseBody?>?

}