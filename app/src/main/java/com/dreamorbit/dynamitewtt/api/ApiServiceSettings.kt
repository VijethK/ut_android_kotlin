package com.dreamorbit.dynamitewtt.api

import android.content.res.Resources
import com.dreamorbit.dynamitewtt.R
import com.dreamorbit.dynamitewtt.application.PhAwareApplication.Companion.context

class ApiServiceSettings {

    private var sBaseApi: String? = null
    private var sStudyToken: String? = null

    constructor() {
        val resources = context.resources
        setBaseApi(resources)
        setStudyToken(resources)
    }

    fun getStudyToken(): String? {
        return sStudyToken
    }

    private fun setStudyToken(resources: Resources) {
        sStudyToken = resources.getString(R.string.study_token_server)
    }

    fun getBaseApi(): String? {
        return sBaseApi
    }

    private fun setBaseApi(resources: Resources) {
        sBaseApi = resources.getString(R.string.base_api)
    }
}