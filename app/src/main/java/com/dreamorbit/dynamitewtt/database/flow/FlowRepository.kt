package com.dreamorbit.dynamitewtt.database.flow

import android.content.ContentValues
import android.content.Context
import androidx.annotation.NonNull
import com.dreamorbit.dynamitewtt.application.PhAwareApplication
import com.dreamorbit.dynamitewtt.database.DbHelper
import com.dreamorbit.dynamitewtt.database.DbTableColumns

class FlowRepository  : IFlowSource{

    private var INSTANCE: FlowRepository? = null

    private var mDbHelper: DbHelper? = null

    // Prevent direct instantiation.
    private fun FlowRepository(@NonNull context: Context): FlowRepository? {
        checkNotNull(context)
        mDbHelper = DbHelper(context)
        return FlowRepository()
    }

    fun getInstance(): FlowRepository? {
        if (INSTANCE == null) {
            INSTANCE = PhAwareApplication().getMyAppContext()?.let { FlowRepository(it) }
        }
        return INSTANCE
    }


   override fun getAssentStatus(screenName: String?): Boolean {
        var status = false
        val db = mDbHelper!!.readableDatabase
        val projection = arrayOf<String>(
            DbTableColumns.Flow.STATUS
        )
        val selectionArgs = arrayOf(
            screenName
        )
        val c = db.query(
            DbTableColumns.Flow.TABLE_NAME,
            projection,
            DbTableColumns.Flow.SCREEN_NAME.toString() + "=?",
            selectionArgs,
            null,
            null,
            null
        )
        if (c != null && c.count > 0) {
            while (c.moveToNext()) {
                status = c.getInt(c.getColumnIndexOrThrow(DbTableColumns.Flow.STATUS)) != 0
            }
        }
        c?.close()
        db.close()
        return status
    }
    override fun saveFlow(flow_status: String?, status: Boolean): Long {
        checkNotNull(status)
        val db = mDbHelper!!.writableDatabase
        val values = ContentValues()
        values.put(DbTableColumns.Flow.SCREEN_NAME, flow_status)
        values.put(DbTableColumns.Flow.STATUS, status)
        val row = db.insert(DbTableColumns.Flow.TABLE_NAME, null, values)
        db.close()
        return row
    }

    override fun deleteFlow() {
        val db = mDbHelper!!.writableDatabase
        db.delete(DbTableColumns.Flow.TABLE_NAME, null, null)
        db.close()
    }

}