package com.dreamorbit.dynamitewtt.database.registration

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import androidx.annotation.NonNull
import com.dreamorbit.dynamitewtt.application.PhAwareApplication
import com.dreamorbit.dynamitewtt.application.PhAwareApplication.Companion.context
import com.dreamorbit.dynamitewtt.database.DbHelper
import com.dreamorbit.dynamitewtt.database.DbTableColumns
import com.dreamorbit.dynamitewtt.database.joinstudy.StudyRepository

class RegistartionRepository : IRegistrationDataSource {

    companion object {
        lateinit var INSTANCE: RegistartionRepository
        lateinit var mDbHelper: DbHelper
            private set
    }
    constructor() {
        INSTANCE = this
        mDbHelper = DbHelper(context)

    }

    @Synchronized
    fun getInstance(): RegistartionRepository = INSTANCE

  /*  fun getInstance(): RegistartionRepository? {
        if (INSTANCE == null) {
            INSTANCE = PhAwareApplication()?.let { RegistartionRepository(it) }
        }
        return INSTANCE
    }*/
    override fun getRegistrationEmailSha(): String? {
        var emailSha: String? = null
        val db = mDbHelper!!.readableDatabase
        val projection = arrayOf<String>(
            DbTableColumns.Registartion.COLUMN_NAME_EMAIL_SHA
        )
        val c = db.query(
            DbTableColumns.Registartion.TABLE_NAME, projection, null, null, null, null, null
        )
        if (c != null && c.count > 0) {
            while (c.moveToNext()) {
                emailSha =
                    c.getString(c.getColumnIndexOrThrow(DbTableColumns.Registartion.COLUMN_NAME_EMAIL_SHA))
            }
        }
        c?.close()
        db.close()
        return emailSha
    }
    override fun getRegistrationEmail(): String? {
        var email: String? = null
        val db = mDbHelper!!.readableDatabase
        val projection = arrayOf<String>(
            DbTableColumns.Registartion.COLUMN_NAME_EMAIL
        )
        val c = db.query(
            DbTableColumns.Registartion.TABLE_NAME, projection, null, null, null, null, null
        )
        if (c != null && c.count > 0) {
            while (c.moveToNext()) {
                email =
                    c.getString(c.getColumnIndexOrThrow(DbTableColumns.Registartion.COLUMN_NAME_EMAIL))
            }
        }
        c?.close()
        db.close()
        return email
    }
    override fun saveRegistrationData(registrationData: RegistartionData?): Long {
        val db = mDbHelper!!.writableDatabase
        val values = ContentValues()
        values.put(DbTableColumns.Registartion.COLUMN_NAME_EMAIL, registrationData!!.getEmail())
        values.put(
            DbTableColumns.Registartion.COLUMN_NAME_EMAIL_SHA,
            registrationData!!.getEmail_sha()
        )
        values.put(DbTableColumns.Registartion.COLUMN_NAME_USERNAME, registrationData!!.getUserName())
        val row = db.insert(DbTableColumns.Registartion.TABLE_NAME, null, values)
        db.close()
        return row
    }

    override fun deleteRegistrationData() {
        val db: SQLiteDatabase = mDbHelper!!.writableDatabase
        db.delete(DbTableColumns.Registartion.TABLE_NAME, null, null)
        db.close()
    }
}