package com.dreamorbit.dynamitewtt.database.flow

interface IFlowSource {

    fun getAssentStatus(screenName: String?): Boolean

    fun saveFlow(screenName: String?, status: Boolean): Long

    fun deleteFlow()

}