package com.dreamorbit.dynamitewtt.database

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class DbHelper(context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {

    companion object {
        val DATABASE_VERSION = 1

        val DATABASE_NAME = "Tasks.db"

        private val TEXT_TYPE = " TEXT"

        private val INTEGER_TYPE = " INTEGER"

        private val LONG_TYPE = " LONG"

        private val DOUBLE_TYPE = " DOUBLE"

        private val BOOLEAN_TYPE = " INTEGER"

        private val COMMA_SEP = ","
    }

    private val SQL_CREATE_JOIN_STUDY =
        "CREATE TABLE " + DbTableColumns.JoinStudy.TABLE_NAME.toString() + " (" +
                DbTableColumns.JoinStudy.COLUMN_NAME_ID.toString() + TEXT_TYPE.toString() + " PRIMARY KEY," +
                DbTableColumns.JoinStudy.COLUMN_NAME_YEAR.toString() + INTEGER_TYPE.toString() + COMMA_SEP +
                DbTableColumns.JoinStudy.COLUMN_NAME_MONTH.toString() + INTEGER_TYPE.toString() + COMMA_SEP +
                DbTableColumns.JoinStudy.COLUMN_NAME_DAY.toString() + INTEGER_TYPE.toString() + COMMA_SEP +
                DbTableColumns.JoinStudy.COLUMN_NAME_AGE.toString() + TEXT_TYPE.toString() + COMMA_SEP +
                DbTableColumns.JoinStudy.COLUMN_NAME_STATUS.toString() + TEXT_TYPE.toString() +
                " )"

    private val SQL_CREATE_REGISTRATION =
        "CREATE TABLE " + DbTableColumns.Registartion.TABLE_NAME + " (" +
                DbTableColumns.Registartion._ID + INTEGER_TYPE.toString() + " PRIMARY KEY," +
                DbTableColumns.Registartion.COLUMN_NAME_EMAIL + INTEGER_TYPE.toString() + COMMA_SEP +
                DbTableColumns.Registartion.COLUMN_NAME_EMAIL_SHA + INTEGER_TYPE.toString() + COMMA_SEP +
                DbTableColumns.Registartion.COLUMN_NAME_USERNAME + INTEGER_TYPE.toString() +
                " )"

    private val SQL_CREATE_FLOW =
        "CREATE TABLE " + DbTableColumns.Flow.TABLE_NAME + " (" +
                DbTableColumns.Flow.SCREEN_NAME + TEXT_TYPE.toString() + COMMA_SEP +
                DbTableColumns.Flow.STATUS + BOOLEAN_TYPE.toString() +
                " )"

    private val SQL_ADDITIONALINFORMATION_FLOW =
        "CREATE TABLE " + DbTableColumns.AdditionalInformation.TABLE_NAME + " (" +
                DbTableColumns.AdditionalInformation._ID.toString() + INTEGER_TYPE.toString() + " PRIMARY KEY," +
                DbTableColumns.AdditionalInformation.GENDER + TEXT_TYPE.toString() + COMMA_SEP +
                DbTableColumns.AdditionalInformation.HEIGHT + TEXT_TYPE.toString() + COMMA_SEP +
                DbTableColumns.AdditionalInformation.WEIGHT + TEXT_TYPE.toString() + COMMA_SEP +
                DbTableColumns.AdditionalInformation.DIAGNOISED_PH + TEXT_TYPE.toString() + COMMA_SEP +
                DbTableColumns.AdditionalInformation.MEDICATION_PH + TEXT_TYPE.toString() + COMMA_SEP +
                DbTableColumns.AdditionalInformation.WEARABLE + TEXT_TYPE.toString() +
                " )"

    private val SQL_CREATE_ACTIVITIES =
        "CREATE TABLE " + DbTableColumns.Activities.TABLE_NAME + " (" +
                DbTableColumns.Activities._ID.toString() + INTEGER_TYPE.toString() + " PRIMARY KEY," +
                DbTableColumns.Activities.NAME + TEXT_TYPE.toString() + COMMA_SEP +
                DbTableColumns.Activities.SCHEDULE_ID + INTEGER_TYPE.toString() + COMMA_SEP +
                DbTableColumns.Activities.ACTIVITY_TYPE_ID + INTEGER_TYPE.toString() + COMMA_SEP +
                DbTableColumns.Activities.CREATED_AT + TEXT_TYPE.toString() + COMMA_SEP +
                DbTableColumns.Activities.UPDATED_AT + TEXT_TYPE.toString() + COMMA_SEP +
                DbTableColumns.Activities.USER_GROUP_ID + INTEGER_TYPE.toString() + COMMA_SEP +
                DbTableColumns.Activities.ACTIVITY_ANSWERED + INTEGER_TYPE.toString() +
                " )"

    private val SQL_CREATE_QUESTIONS =
        "CREATE TABLE " + DbTableColumns.Questions.TABLE_NAME + " (" +
                DbTableColumns.Questions._ID.toString() + INTEGER_TYPE.toString() + " PRIMARY KEY," +
                DbTableColumns.Questions.IDENTIFIER + TEXT_TYPE.toString() + COMMA_SEP +
                DbTableColumns.Questions.FIELD_TYPE_ID + INTEGER_TYPE.toString() + COMMA_SEP +
                DbTableColumns.Questions.ACTIVITY_ID + INTEGER_TYPE.toString() + COMMA_SEP +
                DbTableColumns.Questions.QUESTION + TEXT_TYPE.toString() +
                " )"

    private val SQL_CREATE_QUESTIONS_OPTIONS =
        "CREATE TABLE " + DbTableColumns.QuestionOptions.TABLE_NAME + " (" +
                DbTableColumns.QuestionOptions._ID.toString() + INTEGER_TYPE.toString() + " PRIMARY KEY," +
                DbTableColumns.QuestionOptions.NAME + TEXT_TYPE.toString() + COMMA_SEP +
                DbTableColumns.QuestionOptions.IMAGE + INTEGER_TYPE.toString() + COMMA_SEP +
                DbTableColumns.QuestionOptions.QUESTION_ID + INTEGER_TYPE.toString() +
                " )"

    private val SQL_CREATE_REPORT_SUMMARY =
        "CREATE TABLE " + DbTableColumns.ReportSummary.TABLE_NAME + " (" +
                DbTableColumns.ReportSummary._ID.toString() + INTEGER_TYPE.toString() + " PRIMARY KEY," +
                DbTableColumns.ReportSummary.USER_NAME + TEXT_TYPE.toString() + COMMA_SEP +
                DbTableColumns.ReportSummary.AVERAGE_HEART_RATE + INTEGER_TYPE.toString() + COMMA_SEP +
                DbTableColumns.ReportSummary.MAX_HEART_RATE + INTEGER_TYPE.toString() + COMMA_SEP +
                DbTableColumns.ReportSummary.DISTANCE + DOUBLE_TYPE.toString() + COMMA_SEP +
                DbTableColumns.ReportSummary.STEPS_COUNT + LONG_TYPE.toString() + COMMA_SEP +
                DbTableColumns.ReportSummary.DURATION_OF_TEST + TEXT_TYPE.toString() + COMMA_SEP +
                DbTableColumns.ReportSummary.TEST_TAKEN_AT + TEXT_TYPE.toString() +
                " )"

    private val SQL_CREATE_SCHEDULER =
        "CREATE TABLE " + DbTableColumns.Scheduler.TABLE_NAME + " (" +
                DbTableColumns.Scheduler._ID.toString() + INTEGER_TYPE.toString() + " PRIMARY KEY," +
                DbTableColumns.Scheduler.SCHEDULER_SEQUENCE + TEXT_TYPE.toString() + COMMA_SEP +
                DbTableColumns.Scheduler.SCHEDULER_TIME + TEXT_TYPE.toString() + COMMA_SEP +
                DbTableColumns.Scheduler.SCHEDULER_DAY + TEXT_TYPE.toString() + COMMA_SEP +
                DbTableColumns.Scheduler.SCHEDULER_STATUS + TEXT_TYPE.toString() + COMMA_SEP +
                DbTableColumns.Scheduler.SURVEY_ID + INTEGER_TYPE.toString() + COMMA_SEP +
                DbTableColumns.Scheduler.SURVEY_NAME + TEXT_TYPE.toString() + COMMA_SEP +
                DbTableColumns.Scheduler.TEST_TAKEN_AT + TEXT_TYPE.toString() +
                " )"


override fun onCreate(db: SQLiteDatabase) {
   db.execSQL(SQL_CREATE_JOIN_STUDY)
    db.execSQL(SQL_CREATE_REGISTRATION)
    /*db.execSQL(DbHelper.SQL_CREATE_FLOW)
    db.execSQL(DbHelper.SQL_ADDITIONALINFORMATION_FLOW)
    db.execSQL(DbHelper.SQL_CREATE_ACTIVITIES)
    db.execSQL(DbHelper.SQL_CREATE_QUESTIONS)
    db.execSQL(DbHelper.SQL_CREATE_QUESTIONS_OPTIONS)
    db.execSQL(DbHelper.SQL_CREATE_REPORT_SUMMARY)
    db.execSQL(DbHelper.SQL_CREATE_SCHEDULER)*/
}

override fun onUpgrade(
    db: SQLiteDatabase?,
    oldVersion: Int,
    newVersion: Int
) { // Not required as at version 1
}

override fun onDowngrade(
    db: SQLiteDatabase?,
    oldVersion: Int,
    newVersion: Int
) { // Not required as at version 1
}
}