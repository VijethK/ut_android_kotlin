package com.dreamorbit.dynamitewtt.database.joinstudy

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import androidx.annotation.NonNull
import com.dreamorbit.dynamitewtt.application.PhAwareApplication
import com.dreamorbit.dynamitewtt.database.DbHelper
import com.dreamorbit.dynamitewtt.database.DbTableColumns

class StudyRepository : IStudyDataSource {
    companion object {
        private var INSTANCE: StudyRepository? = null

        private var mDbHelper: DbHelper? = null
    }
    /**
     * Prevent direct instantiation.
     */
    private fun StudyRepository(@NonNull context: Context): StudyRepository? {
        checkNotNull(context)
        mDbHelper = DbHelper(context)
        return StudyRepository()
    }

    /**
     * Singleton class to create a single instance for this class
     *
     * @return INSTANCE
     */
    fun getInstance(): StudyRepository? {
        if (INSTANCE == null) {
            INSTANCE = PhAwareApplication().getMyAppContext()?.let { StudyRepository(it) }
        }
        return INSTANCE
    }


    override fun getStudyData(): StudyData? {
        var studyData: StudyData? = null
        val db: SQLiteDatabase = mDbHelper!!.getReadableDatabase()
        val projection =
            arrayOf<String>( //DbTableColumns.JoinStudy.COLUMN_NAME_ID,
                DbTableColumns.JoinStudy.COLUMN_NAME_YEAR,
                DbTableColumns.JoinStudy.COLUMN_NAME_MONTH,
                DbTableColumns.JoinStudy.COLUMN_NAME_DAY,
                DbTableColumns.JoinStudy.COLUMN_NAME_AGE,
                DbTableColumns.JoinStudy.COLUMN_NAME_STATUS
            )
        val c = db.query(
            DbTableColumns.JoinStudy.TABLE_NAME, projection, null, null, null, null, null
        )
        if (c != null && c.count > 0) {
            while (c.moveToNext()) { //String Id = c.getSignature(c.getColumnIndexOrThrow(DbTableColumns.JoinStudy.COLUMN_NAME_ID));
                val year =
                    c.getInt(c.getColumnIndexOrThrow(DbTableColumns.JoinStudy.COLUMN_NAME_YEAR))
                val month =
                    c.getInt(c.getColumnIndexOrThrow(DbTableColumns.JoinStudy.COLUMN_NAME_MONTH))
                val day =
                    c.getInt(c.getColumnIndexOrThrow(DbTableColumns.JoinStudy.COLUMN_NAME_DAY))
                val age =
                    c.getString(c.getColumnIndexOrThrow(DbTableColumns.JoinStudy.COLUMN_NAME_AGE))
                val status =
                    c.getInt(c.getColumnIndexOrThrow(DbTableColumns.JoinStudy.COLUMN_NAME_STATUS)) == 1
                studyData = StudyData(year, month, day, age, status)
            }
        }
        c?.close()
        db.close()
        return studyData
    }

    override fun saveStudyData(studyData: StudyData?) {
        checkNotNull(studyData)
        val db: SQLiteDatabase = mDbHelper!!.getWritableDatabase()
        val values = ContentValues()
        values.put(DbTableColumns.JoinStudy.COLUMN_NAME_YEAR, studyData.getYear())
        values.put(DbTableColumns.JoinStudy.COLUMN_NAME_MONTH, studyData.getMonth())
        values.put(DbTableColumns.JoinStudy.COLUMN_NAME_DAY, studyData.getDay())
        values.put(DbTableColumns.JoinStudy.COLUMN_NAME_AGE, studyData.getAge())
        values.put(DbTableColumns.JoinStudy.COLUMN_NAME_STATUS, studyData.getStatus())
        db.insert(DbTableColumns.JoinStudy.TABLE_NAME, null, values)
        db.close()
    }

    override fun deleteStudyData() {
        val db: SQLiteDatabase = mDbHelper!!.getWritableDatabase()
        db.delete(DbTableColumns.JoinStudy.TABLE_NAME, null, null)
        db.close()
    }
}