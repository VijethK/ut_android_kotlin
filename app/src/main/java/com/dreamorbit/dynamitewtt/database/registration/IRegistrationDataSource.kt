package com.dreamorbit.dynamitewtt.database.registration

import androidx.annotation.NonNull

interface IRegistrationDataSource {

    fun getRegistrationEmailSha(): String?

    fun getRegistrationEmail(): String?

    fun saveRegistrationData(@NonNull studyData: RegistartionData?): Long

    fun deleteRegistrationData()
}