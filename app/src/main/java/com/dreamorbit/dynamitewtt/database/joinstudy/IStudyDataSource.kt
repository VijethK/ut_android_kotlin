package com.dreamorbit.dynamitewtt.database.joinstudy

import androidx.annotation.NonNull

interface IStudyDataSource {

    fun getStudyData(): StudyData?

    fun saveStudyData(@NonNull studyData: StudyData?)

    fun deleteStudyData()
}