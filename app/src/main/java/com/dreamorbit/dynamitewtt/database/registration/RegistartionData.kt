package com.dreamorbit.dynamitewtt.database.registration

class RegistartionData {
    private var email: String? = null
    private var email_sha: String? = null
    private var userName: String? = null

    fun RegistartionData() {}

    fun RegistartionData(
        email: String?,
        email_sha: String?,
        userName: String?
    ) {
        this.email = email
        this.email_sha = email_sha
        this.userName = userName
    }

    fun getEmail(): String? {
        return email
    }

    fun setEmail(email: String?) {
        this.email = email
    }

    fun getEmail_sha(): String? {
        return email_sha
    }

    fun setEmail_sha(email_sha: String?) {
        this.email_sha = email_sha
    }

    fun getUserName(): String? {
        return userName
    }

    fun setUserName(userName: String?) {
        this.userName = userName
    }
}