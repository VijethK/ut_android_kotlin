package com.dreamorbit.dynamitewtt.database.joinstudy

class StudyData(
    private  var year: Int, private var month: Int,
    private  var day: Int,
    private  var age: String?,
    private  var status: Boolean
) {
    private val studyID: String? = null



    /*public String getStudyID() {
        return studyID;
    }

    public void setStudyID(String studyID) {
        this.studyID = studyID;
    }*/

    /*public String getStudyID() {
        return studyID;
    }

    public void setStudyID(String studyID) {
        this.studyID = studyID;
    }*/
    fun getAge(): String? {
        return age
    }

    fun setAge(age: String?) {
        this.age = age
    }

    fun getStatus(): Boolean {
        return status
    }

    fun setStatus(status: Boolean) {
        this.status = status
    }


    fun getYear(): Int {
        return year
    }

    fun setYear(year: Int) {
        this.year = year
    }

    fun getMonth(): Int {
        return month
    }

    /*  public void setMonth(int month) {
        this.month = month;
    }*/

    /*  public void setMonth(int month) {
        this.month = month;
    }*/
    fun getDay(): Int {
        return day
    }

    fun setDay(day: Int) {
        this.day = day
    }
}