package com.dreamorbit.dynamitewtt.database

import android.provider.BaseColumns

class DbTableColumns {
    // To prevent someone from accidentally instantiating the contract class,
// give it an empty constructor.
    private fun DbTableColumns() {}

    /* Inner class that defines the hoinstudy table contents */
    object JoinStudy : BaseColumns {
        const val TABLE_NAME = "join_study_details"
        const val COLUMN_NAME_ID = "study_id"
        const val COLUMN_NAME_YEAR = "year"
        const val COLUMN_NAME_MONTH = "month"
        const val COLUMN_NAME_DAY = "day"
        const val COLUMN_NAME_STATUS = "study_status"
        const val COLUMN_NAME_AGE = "age"
    }
    /* Inner class that defines the register table contents */
    object Registartion : BaseColumns {
        const val _ID = BaseColumns._ID
        const val TABLE_NAME = "registration_details"
        const val COLUMN_NAME_EMAIL = "email"
        const val COLUMN_NAME_EMAIL_SHA = "email_sha"
        const val COLUMN_NAME_USERNAME = "user_name"
    }

    /* Inner class that defines the flow table contents */
    object Flow : BaseColumns {
        const val TABLE_NAME = "flow"
        const val SCREEN_NAME = "screen_name"
        const val STATUS = "status"
    }

    /* Inner class that defines the additional information contents */
    object AdditionalInformation : BaseColumns {
        const val _ID = BaseColumns._ID
        const val TABLE_NAME = "additional_information"
        const val GENDER = "gender"
        const val HEIGHT = "height"
        const val WEIGHT = "weight"
        const val DIAGNOISED_PH = "diagnoised_ph"
        const val MEDICATION_PH = "medication_ph"
        const val WEARABLE = "wearable"
    }

    /* Inner class that defines the activities contents */
    object Activities : BaseColumns {
        const val _ID = BaseColumns._ID
        const val TABLE_NAME = "activity_list"
        const val NAME = "name"
        const val SCHEDULE_ID = "schedule_id"
        const val ACTIVITY_TYPE_ID = "activity_type_id"
        const val CREATED_AT = "created_at"
        const val UPDATED_AT = "updated_at"
        const val USER_GROUP_ID = "user_group_id"
        const val ACTIVITY_ANSWERED = "activity_answered"
    }

    /* Inner class that defines the questions contents */
    object Questions : BaseColumns {
        const val _ID = BaseColumns._ID
        const val TABLE_NAME = "questions_list"
        const val IDENTIFIER = "identifier"
        const val FIELD_TYPE_ID = "field_type_id"
        const val ACTIVITY_ID = "activity_id"
        const val QUESTION = "question"
    }

    /* Inner class that defines the questions contents */
    object QuestionOptions : BaseColumns {
        const val _ID = BaseColumns._ID
        const val TABLE_NAME = "option_list"
        const val NAME = "name"
        const val IMAGE = "laying"
        const val QUESTION_ID = "activity_id"
    }

    /* Inner class that defines the questions contents */
    object ReportSummary : BaseColumns {
        const val _ID = BaseColumns._ID
        const val TABLE_NAME = "report_summary"
        const val USER_NAME = "user_name"
        const val AVERAGE_HEART_RATE = "avg_heart_rate"
        const val MAX_HEART_RATE = "max_heart_rate"
        const val DISTANCE = "distance"
        const val STEPS_COUNT = "steps_count"
        const val DURATION_OF_TEST = "duration_of_test"
        const val TEST_TAKEN_AT = "test_taken_at"
        const val FLOOR_ASCENDED = "floor_ascended"
    }

    /* Inner class that defines the questions contents */
    object Scheduler : BaseColumns {
        const val _ID = BaseColumns._ID
        const val TABLE_NAME = "scheduler_table"
        const val SCHEDULER_STATUS = "scheduler_status"
        const val TEST_TAKEN_AT = "test_taken_at"
        const val SCHEDULER_TIME = "scheduler_time"
        const val SCHEDULER_DAY = "scheduler_day"
        const val SCHEDULER_SEQUENCE = "scheduler_sequence"
        const val SURVEY_ID = "survey_id"
        const val SURVEY_NAME = "survey_name"
    }
}