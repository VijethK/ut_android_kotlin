package com.dreamorbit.dynamitewtt.application

class AppConstants {


    val TASK_TAG_PERIODIC = "periodic_task"

    //Header Font
//public static final String APP_FONT = "ChicaGogoNF.ttf";
    val APP_FONT_DEFAULT = "proximaNova-Regular.ttf"

    //Bundle Key Names
    val IS_ELIGIBLE = "is_eligible"
    val YEAR = "year"
    val MONTH = "month"
    val DAY = "day"
    val AGE = "age"

    //Screen Flow Names
    val REGISTER_FLOW = "registerFlow"
    //public static final String ASSENT_FLOW = "assentFlow";
//public static final String CONSENT_FLOW = "consentFlow";
    val CONSENT_ONE_FLOW = "consent_one_flow"
    val CONSENT_TWO_FLOW = "consent_two_flow"
    val ADDITIONAL_INFO_FLOW = "additionalInfoFlow"
    //public static final String ACTIVITY_FLOW = "activityFlow";


    //public static final String ACTIVITY_FLOW = "activityFlow";
//public static final String ADDITIONAL_INFO = "additionalInfo";
    val ACTIVITY_ID_ARRAY = "activity_id_array"

    val HTML_FILE = "html_file"
    val PAGE_TITLE = "page_title"

    //Intent service
    val BACKGROUND_ACTION = "background_action"

    //Alarm to reset Activities everyday morning 7am
    val REPEAT_HOUR = 7
    val REPEAT_MIN = 0
    //public static final int REPEAT_APM_PM = Calendar.AM;

    //public static final int REPEAT_APM_PM = Calendar.AM;
    val PROFILE_ABOUT = "profile_about"

    val LOGIN = "login"
    //public static final String IMMEDIATELY = "immediately";

    //public static final String IMMEDIATELY = "immediately";
//Wear start/stop constant values
    val APP_LOGINED = 0
    val APP_LOGOUT = 1
    val ZIP_FILE_RECEIVED = 2
    val LAUNCH_WEAR = 3
    val TEST_SCHEDULER_ENABLE = 4
    val WEAR_MESSAGE_EMERGENCY_CALL = "WEAR_MESSAGE_EMERGENCY_CALL"
    val RATING_ALERT = "RATING_ALERT"
    val DEVICE_TYPE = "android"
    val TEST_SEQUENCE = "test_sequence"
    val SURVEY_ID = "survey_id"
    val SURVEY_NAME = "survey_name"

    val WEAR_MESSAGE_SCHEDULER_STATUS = "scheduler_test_status"
    val ACTIVITY_HAS_RESET = "activity_has_reset"
    val RATE_APP = "rate_app"
    val OFFLINE_ALERT = "offline_alert"
}