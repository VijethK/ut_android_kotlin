package com.dreamorbit.dynamitewtt.application

import android.content.Context
import androidx.multidex.MultiDex
import androidx.multidex.MultiDexApplication
import com.dreamorbit.dynamitewtt.custom.FontsOverride
import com.dreamorbit.dynamitewtt.network.ConnectivityReceiver
import com.dreamorbit.dynamitewtt.utilities.SharedPrefSingleton

public open class PhAwareApplication : MultiDexApplication() {


    companion object {
        lateinit var mInstance: PhAwareApplication
         lateinit var context: Context
            private set
    }

    fun getMyAppContext(): Context = context

    @Synchronized
    fun getInstance(): PhAwareApplication = mInstance

    override fun onCreate() {
        super.onCreate()
        context = applicationContext
        mInstance = this
       /* //Twitter initialization
        val config: TwitterConfig = Builder(this)
            .logger(DefaultLogger(Log.DEBUG))
            .twitterAuthConfig(
                TwitterAuthConfig(
                    getString(R.string.CONSUMER_KEY),
                    getString(R.string.CONSUMER_SECRET)
                )
            )
            .debug(true)
            .build()
        Twitter.initialize(config)*/
        //CustomFonts
        //CustomFonts
        val appConstants = AppConstants()
        val fontsOverride = FontsOverride()
        fontsOverride.setDefaultFont(applicationContext, "MONOSPACE", "ChicaGogoNF.ttf")
        fontsOverride.setDefaultFont(applicationContext, "DEFAULT", appConstants.APP_FONT_DEFAULT)
        //Fabric.with(this, Crashlytics())
        SharedPrefSingleton.getInstance().initialize(this)
        //SdkSharedPrefSingleton.getInstance().initialize(this, SdkSharedPrefSingleton.ASSENT_PREFERENCE)
        //ApiServiceSettings(this)
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }



    /**
     * Setting the listener for the broadcast reciver
     * to receive updates [ ][listener]
     */
    open fun setConnectivityListener(listener: ConnectivityReceiver.ConnectivityReceiverListener?) {
        val connectivityReceiver = ConnectivityReceiver()
        connectivityReceiver.connectivityReceiverListener = listener
    }
}