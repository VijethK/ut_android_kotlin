package com.dreamorbit.dynamitewtt.network

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import com.dreamorbit.dynamitewtt.application.PhAwareApplication
import java.io.File

class ConnectivityReceiver : BroadcastReceiver() {
    val ROOT_APP_FOLDER_NAME = "Phaware"

   public var connectivityReceiverListener: ConnectivityReceiver.ConnectivityReceiverListener? =
        null

    fun ConnectivityReceiver() {
    }

    fun isConnected(): Boolean {
        val phawareApplication = PhAwareApplication()
        val cm =
            phawareApplication.getMyAppContext()!!.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork = cm.activeNetworkInfo
        return (activeNetwork != null
                && activeNetwork.isConnectedOrConnecting)
    }

    override fun onReceive(context: Context, arg1: Intent?) {
        val cm = context
            .getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork = cm.activeNetworkInfo
        val isConnected = (activeNetwork != null
                && activeNetwork.isConnectedOrConnecting)
        if (connectivityReceiverListener != null) {
            connectivityReceiverListener!!.onNetworkConnectionChanged(isConnected)
        }
        //Check if any pending zip files available in app folder
      /*  val directories: Array<File> = getAllDirectories(context)
        if (isConnected && directories != null && directories.size > 0) {
            val intent = Intent(context, BackGroundService::class.java)
            intent.putExtra(
                AppConstants.BACKGROUND_ACTION,
                String.valueOf(Utility.BackgroundAction.upload_all_pending)
            )
            BackGroundService.enqueueBackgroundWork(context, intent)
        }*/
    }

    /*from SDKUtil Class*/
    val PH_PDF_DOCUMENTS: kotlin.String? = "Ph_PDF"
    fun getAllDirectories(context: Context): Array<File> {
        val myDirectory =
            File(context.filesDir.absolutePath + File.separator + ROOT_APP_FOLDER_NAME)
        if (!myDirectory.exists()) myDirectory.mkdir()
        return myDirectory.listFiles()
    }

    interface ConnectivityReceiverListener {
        fun onNetworkConnectionChanged(isConnected: Boolean)
    }
}